package com.llw.changeavatar;



import com.llw.changeavatar.utils.Const;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.hutool.core.map.MapUtil;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void sds() {
        HttpResponse httpResponse=HttpUtil.createGet(Const.PUBLIC_SERVER+"/api/getcaptcha").execute();
        String Sid=httpResponse.header("sid");
        String user ="12345677";
        String pwd ="12345677";
        Map<String, String> map = MapUtil.builder(new HashMap<String, String>()).
                put("username", user).
                put("userpwd", pwd).
                put("code", "sfse").
                put("sid", "beb2y578099").
                build();
        String SSS = HttpUtil.createPost(Const.PUBLIC_SERVER+"/api/user/sturegister").formStr(map).execute().body();
        JSONObject jsonObject = JSONUtil.parseObj(SSS);
        jsonObject.getShort("code");
        System.out.println(SSS);
    }
    @Test
    public void addition_isCorrect() {
        String str = "05-05 14:36:32.315 13656 14656 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:7c3647eb3de64c2cb3ff57fcceda5ff5\n" +
                "05-05 14:36:58.738 13656 14656 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:71c6d861cac84e1292084a96a22d5049\n" +
                "05-05 14:37:24.802 13656 14656 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:1a633932c78b482da5f0595c028d6bdf\n" +
                "05-05 14:37:51.157 13656 14656 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:c3d46bfb5bfb4cbfb02f388aa121ff32\n" +
                "05-05 14:38:17.660 13656 14656 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:606319ee14a3495e9fdf61742b670c0d\n" +
                "05-05 14:38:44.672 17264 17304 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:bee265976a424df68a55f0119c178dde\n" +
                "05-05 14:39:10.856 17264 17304 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:9e4d303d83164787a79024a91627b1ef\n" +
                "05-05 14:39:37.247 17264 17304 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:9a5d7158bf40ca8a7a8d4de7bb10b0\n" +
                "05-05 14:40:04.771 17264 17304 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:a5b4da0205e4a7fa8d55bec27a5078c\n" +
                "05-05 14:40:31.428 17264 17304 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:dd622e47048441dca868e305b4fbd403\n" +
                "05-05 14:40:57.830 17264 17304 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:fbfeb126e7204a59af94c25379a35150\n" +
                "05-05 14:41:23.964 17264 17304 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:9419db4ba3c940f7aeafcc13138cdb5a\n" +
                "05-05 14:41:51.616 17264 17304 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:e2160feb9f1a47c09c61ab0615e4358b\n" +
                "05-05 14:42:18.272 17264 17304 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:609063e9f56d4fab9d9792ec85052aac\n" +
                "05-05 14:42:44.951 17264 17304 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:ea03dee3cc5e41fcbfa51798e85c44c8\n" +
                "05-05 14:43:13.492 21292 21328 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:1ede15082b464a3bba940111f11d71ae\n" +
                "05-05 14:43:40.772 21292 21328 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:fdc7c64e9023451da4e2fcc0237f7090\n" +
                "05-05 14:44:07.272 21292 21328 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:256814f116b74e5eae9ef33a6d7ecbe0\n" +
                "05-05 14:44:33.885 21292 21328 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:7cc93b96a2ac4ad4923fe737594b9c87\n" +
                "05-05 14:45:01.480 24362 24403 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:22f67ed43ee0432c9f681345a34e5238\n" +
                "05-05 14:45:28.138 24362 24403 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:4325ebed234a415fbc6a515236fe1ab8\n" +
                "05-05 14:45:55.545 24362 24403 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:ddf97c028f644723b5e968be5e56606e\n" +
                "05-05 14:46:00.486 24362 24403 I VA_AivsSDK-[AIVS 54-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 14:46:00.486 24362 24403 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:4967a9d5f01d44d7bcad38e8c6cefe5d\n" +
                "05-05 14:46:21.830 24362 24403 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:40d50bc0d89444f6b3db4b29422c2497\n" +
                "05-05 14:46:49.515 24362 24403 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:d57fdb3fc5c64872a9e723f7fa8c7f53\n" +
                "05-05 14:47:15.890 24362 24403 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:10302bb4417e4bf8a77f9b8417ffefb0\n" +
                "05-05 14:47:43.177 27555 27590 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:474ac6450e9540e5bf488cfb93781e75\n" +
                "05-05 14:48:10.410 27555 27590 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:e2cbf708934d45579e2fa1c326d969e6\n" +
                "05-05 14:48:37.731 29050 29083 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:c13abca074bf4c2f9865a8abdca5024b\n" +
                "05-05 14:48:44.846 29050 29083 I VA_AivsSDK-[AIVS 46-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 14:48:44.846 29050 29083 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:85fb8502ed5f4cb5b7ee54e729843d51\n" +
                "05-05 14:49:04.576 29050 29083 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:c909daed74cb4f8d9b91f7ed29734e72\n" +
                "05-05 14:49:11.770 29050 29083 I VA_AivsSDK-[AIVS 46-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 14:49:11.770 29050 29083 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:86beff2601e24f29beda65ef23de08a7\n" +
                "05-05 14:49:31.460 29050 29083 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:f3574ff29f2347ea8e470f1f61a64677\n" +
                "05-05 14:49:59.106 30735 30772 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:a196a4cb9a8943c3b970e449da2c21c5\n" +
                "05-05 14:50:25.263 30735 30772 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:e7c80ac619b94b009e841ca990956b41\n" +
                "05-05 14:50:51.787 30735 30772 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:8025cd5c2a0b4604a67522badfbf1473\n" +
                "05-05 14:51:18.421 30735 30772 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:5f41d686fed24b5cb14b1f22c22e8efc\n" +
                "05-05 14:51:45.542  3538  3572 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:fd05f13907fb421f8da306a6caf66bf0\n" +
                "05-05 14:52:12.155  3538  3572 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:1336098ba4ba409a9bb7588276451ba2\n" +
                "05-05 14:52:40.667  5564  5603 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:a99d518487be40709618c4b8b788687f\n" +
                "05-05 14:53:06.920  5564  5603 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:688dec44d75942cb8a4e0b7b99a8c282\n" +
                "05-05 14:53:34.471  5564  5603 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:a4c8e0a153a149bcb6e37cfc4c96a440\n" +
                "05-05 14:54:01.662  5564  5603 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:fd4edbf4b04a48878f3f4fbd6767b7a8\n" +
                "05-05 14:54:28.288  7672  7712 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:9bb77c2b58414f7db813ebcf58251faf\n" +
                "05-05 14:54:35.730  7672  7712 I VA_AivsSDK-[AIVS 46-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 14:54:35.730  7672  7712 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:bdf569bc870f47be81d77de76edc0b2e\n" +
                "05-05 14:54:55.684  7672  7712 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:f2b5a577cc504085beabadb0618a6150\n" +
                "05-05 14:55:03.049  7672  7712 I VA_AivsSDK-[AIVS 46-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 14:55:03.049  7672  7712 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:963a595beb6a47fd89ed081fe51ce077\n" +
                "05-05 14:55:21.956  7672  7712 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:f72eb1665534a1b94f42a2f5616c16e\n" +
                "05-05 14:55:50.585  9312  9351 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:61f0e216a1724a079aa91707425e855f\n" +
                "05-05 14:56:17.736 10944 10978 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:8808dc6fdf854cc4b4bae5d48be66270\n" +
                "05-05 14:56:44.784 10944 10978 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:ba0282671e6c45ee9916569e9c143f0c\n" +
                "05-05 14:57:11.988 12628 12666 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:c219f7a55c14448cbe6d110f0ecc991f\n" +
                "05-05 14:57:38.205 12628 12666 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:db663c4630c24f60b560753fcc2c02fe\n" +
                "05-05 14:58:06.003 13776 13811 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:a1db9ebd1c8f49f4ba2b86f456d5e4bf\n" +
                "05-05 14:58:32.268 13776 13811 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:a61004fbb4ff46ba831e07d204e03248\n" +
                "05-05 14:58:42.073 13776 13811 I VA_AivsSDK-[AIVS 48-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 14:58:42.073 13776 13811 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:56f08946372541b8a783a5d059bec20e\n" +
                "05-05 14:58:59.387 13776 13811 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:42400c787676466a89611930fc8c148d\n" +
                "05-05 14:59:25.888 13776 13811 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:97b2135be5574532acfe10e928f5e411\n" +
                "05-05 14:59:53.684 13776 13811 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:5ee0e80ed78044df94de524874bac36c\n" +
                "05-05 14:59:59.653 13776 13811 I VA_AivsSDK-[AIVS 48-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 14:59:59.654 13776 13811 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:ad7cd03f5ddb4b29aa1b79addfc8a6ac\n" +
                "05-05 15:00:20.383 13776 13811 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:1c9709d42ff04c65998c3fd6439cbb67\n" +
                "05-05 15:00:48.029 15537 15577 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:7aa70f0ea1e84c8cb55c5c1ca00005f2\n" +
                "05-05 15:01:14.506 16508 16543 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:cb6c87013cf3409ba9f5550305aa5144\n" +
                "05-05 15:01:41.688 17430 17464 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:15df155a2070442797a646871fe6a643\n" +
                "05-05 15:01:48.972 17430 17464 I VA_AivsSDK-[AIVS 47-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 15:01:48.972 17430 17464 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:e6a4c243d33f4f8294b23aecf7dd8b07\n" +
                "05-05 15:02:08.142 17430 17464 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:a4086cd016446fe9e3667c09338a05e\n" +
                "05-05 15:02:35.363 17430 17464 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:b00106ed3d8f4f15be7412740eac4e4e\n" +
                "05-05 15:03:02.644 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:79a1de025bf244a3bb6ac4e91768d7bb\n" +
                "05-05 15:03:10.149 19640 19674 I VA_AivsSDK-[AIVS 47-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 15:03:10.150 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:60cfec37a58d46a5917957932a1343f8\n" +
                "05-05 15:03:29.166 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:c3c92a7fe63c4c0489616c79ed3d6e5d\n" +
                "05-05 15:03:56.522 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:8a7e737aeed44e89ab22977e957058ae\n" +
                "05-05 15:04:23.759 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:84871d3a7299408eb1a8a43046b2b696\n" +
                "05-05 15:04:50.399 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:3db7377d16a04bfdb56bcc93c3f7f3dd\n" +
                "05-05 15:05:18.024 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:b885620dae434aff8c82775cc5763658\n" +
                "05-05 15:05:45.566 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:f96997eff78a418f9520d6eecbbad9b7\n" +
                "05-05 15:05:50.674 19640 19674 I VA_AivsSDK-[AIVS 47-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 15:05:50.674 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:d4c8c7e0e44d4f2a91680449940533f8\n" +
                "05-05 15:06:12.372 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:5f8db5c5dddc44dab8778ea67e17cfe8\n" +
                "05-05 15:06:39.116 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:8d6dc954f9074108a2e8700c0dd57806\n" +
                "05-05 15:06:46.494 19640 19674 I VA_AivsSDK-[AIVS 47-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 15:06:46.495 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:d67dc7e302524f118addc6088e4b42ee\n" +
                "05-05 15:07:06.763 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:32f8e300903449a4b9f36360c5ad3908\n" +
                "05-05 15:07:13.960 19640 19674 I VA_AivsSDK-[AIVS 47-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 15:07:13.961 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:e3e2077f999a4f1c836c57607a6047fa\n" +
                "05-05 15:07:32.904 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:9fd0a820d9564f5abae9485f862f9971\n" +
                "05-05 15:08:00.856 19640 19674 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:c627b901c6da4d32aecb135c8282467b\n" +
                "05-05 15:08:27.581 23912 23946 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:59894ae2a8454bfdbe9aa44f1423635f\n" +
                "05-05 15:08:54.876 25454 25488 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:84a9bc7d52fc429ab42a930d3596f730\n" +
                "05-05 15:09:20.964 25454 25488 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:782784067d214b4c9239178b01258ed1\n" +
                "05-05 15:09:49.489 26579 26613 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:474280293baf4f1985b7cb1c00b60469\n" +
                "05-05 15:10:16.491 26579 26613 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:f6ffd361007840d892dfeff63a7cf854\n" +
                "05-05 15:10:23.913 26579 26613 I VA_AivsSDK-[AIVS 47-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 15:10:23.913 26579 26613 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:975866a9b2a54243b8b982cb843be8b1\n" +
                "05-05 15:10:43.326 26579 26613 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:f4a33b6973404f1c92392ad59e372b8f\n" +
                "05-05 15:11:10.882 28021 28053 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:7c2b9fda3eba4aa29bb606171b04c54f\n" +
                "05-05 15:11:37.891 29403 29441 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:d2e28229cb934a11ae7f4d4558cd8e0d\n" +
                "05-05 15:12:05.082 30360 30394 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:a5b6ec5283be4ebcb8e5823c64782446\n" +
                "05-05 15:12:32.153 31295 31331 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:17837f2fae4e4b6bb6ae0c7b1e131b3a\n" +
                "05-05 15:12:39.201 31295 31331 I VA_AivsSDK-[AIVS 49-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 15:12:39.201 31295 31331 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:1f8775ba83a749c4b1cc46f102095b84\n" +
                "05-05 15:12:58.423 31295 31331 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:a17ab556804436aeb41084843cf89f\n" +
                "05-05 15:13:25.175 31295 31331 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:8d3bc2cb4e0a4f89a6dd4d390003979e\n" +
                "05-05 15:13:52.843 31295 31331 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:b08e0f8406c843eab53db784d25ddd02\n" +
                "05-05 15:14:19.603 31295 31331 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:fd1a58ea488c402fa9211e8e7d33dc07\n" +
                "05-05 15:14:46.426 31295 31331 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:a52b61427c4c4e42ac2df7c9299432ea\n" +
                "05-05 15:15:13.885  2117  2195 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:4e092b996dc4cd39bed74764df9d053\n" +
                "05-05 15:15:40.503  2117  2195 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:8bb8bab709974f3d8ca797717068b8b2\n" +
                "05-05 15:16:07.068  2117  2195 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:f9556a5b46524871afe87743acf5687d\n" +
                "05-05 15:16:15.505  2117  2195 I VA_AivsSDK-[AIVS 46-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 15:16:15.505  2117  2195 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:2c0dd145f5c54f93b2793929cd2922eb\n" +
                "05-05 15:16:33.818  2117  2195 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:78003d4bcb4548cd9c97eabf751fa2fe\n" +
                "05-05 15:16:40.907  2117  2195 I VA_AivsSDK-[AIVS 46-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 15:16:40.907  2117  2195 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:ddbcb943e78f471bb673ef51b99e7452\n" +
                "05-05 15:17:00.707  2117  2195 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:cbd7cb122ebe4447a4afa0f5a7752f81\n" +
                "05-05 15:17:27.659  6731  6771 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:c0a67ed013d1436fb33e51d5327c7631\n" +
                "05-05 15:17:54.054  6731  6771 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:fac2ebda725746949517dd75ca360ad6\n" +
                "05-05 15:18:21.308  8563  8601 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:60cdea1e3060481cb5a2a2c043d64690\n" +
                "05-05 15:18:28.165  8563  8601 I VA_AivsSDK-[AIVS 46-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 15:18:28.165  8563  8601 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:46ad3713669b4e2c894d3f5a8113ae0a\n" +
                "05-05 15:18:48.598  8563  8601 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:e53bfe76e96b4f53abe3188e8d92bdcc\n" +
                "05-05 15:18:55.706  8563  8601 I VA_AivsSDK-[AIVS 46-TraceThread] TraceManager: traceRequestId: not startTrace\n" +
                "05-05 15:18:55.706  8563  8601 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:b8e3e05ff2524e6c8c8ea88b4ebdeb4e\n" +
                "05-05 15:19:15.186  8563  8601 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:7931be8498a4ab3bf6944dbc0e45f52\n" +
                "05-05 15:19:42.948 10148 10190 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:d7aed3eb005d4c39991941d5bd1108b8\n" +
                "05-05 15:20:09.219 10148 10190 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:7361addd1e344afebc7f273fee40e28a\n" +
                "05-05 15:20:36.112 10148 10190 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:9c9b376853464a46b46d8287b0ebd54d\n" +
                "05-05 15:21:03.167 10148 10190 D VA_CoreIndicatorsTrace: ***key:traceRequest***value:b469e7229c3f456ca712089fe40742da\n";

        String[] strs=str.split("\n");

        for(int i=0,len=strs.length;i<len;i++){
           String st=strs[i].replaceAll("^.*value:","");
           if(st.length()<40)
            System.out.println("'"+st+"',");
        }
    }
}