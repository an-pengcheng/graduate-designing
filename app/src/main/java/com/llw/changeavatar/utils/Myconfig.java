package com.llw.changeavatar.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Myconfig {
    private static DBUtil dbUtil;
    private static boolean isInit = false;
    private static SQLiteDatabase readableDatabase;
    private static SQLiteDatabase writableDatabase;
    public static  void init(Context context){
        if(!isInit){
            dbUtil = new DBUtil(context,"info.db",null,1);
             readableDatabase = dbUtil.getReadableDatabase();
             writableDatabase = dbUtil.getWritableDatabase();
        }
        isInit = true;
    }
    public static String getValue(String key){
        Cursor queryres = readableDatabase.query("config", new String[]{"key","value"}, "key=?", new String[]{key},null,null,null);
        if(queryres.moveToFirst()){
            String value = queryres.getString(queryres.getColumnIndex("value"));
            return value==null?"":value;
        }
        return "";
    }
    public static boolean SetValue(String key,String value){
        ContentValues values=new ContentValues();
        values.put("value",value);
        values.put("key",key);
        writableDatabase.delete("config","key=?",new String[]{key});
        return writableDatabase.insert("config",null,values)>0;
    }
    public static boolean delateAll(){
        return writableDatabase.delete("config","1=1",new String[]{})>0;
    }
}
