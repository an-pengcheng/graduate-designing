package com.llw.changeavatar.Activity;

import static com.llw.changeavatar.Activity.ButtomTab.activityList;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;


import com.ejlchina.okhttps.HTTP;
import com.ejlchina.okhttps.HttpResult;
import com.ejlchina.okhttps.WebSocket;
import com.llw.changeavatar.Activity.Login.MainActivity;
import com.llw.changeavatar.Activity.Login.SignUp;
import com.llw.changeavatar.Adapter.TalkAdapter;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.TalkBean;
import com.llw.changeavatar.utils.Const;
import com.llw.changeavatar.utils.Myconfig;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;

public class TalkActivity extends AppCompatActivity {

    private ListView msgListView;
    private EditText inputText;
    private Button send;
    private TalkAdapter adapter;
    ImageView shop_pic,user_pic,iv_back;
    private List<TalkBean> msgList = new ArrayList<TalkBean>();
    private WebSocket ws;

    @Override
    protected void onDestroy() {
        ws.close(1001,"bye");
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_talk);
        activityList.add(this);

        adapter = new TalkAdapter(TalkActivity.this, R.layout.talk_item, msgList);
        inputText = (EditText)findViewById(R.id.input_text);
        send = (Button)findViewById(R.id.send);
        msgListView = (ListView)findViewById(R.id.msg_list_view);
        shop_pic=  findViewById(R.id.head_left);
        user_pic=  findViewById(R.id.head_right);
        iv_back=findViewById(R.id.iv_back);
        msgListView.setAdapter(adapter);
        initMsgs();
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String content = inputText.getText().toString();

                if(!"".equals(content)) {
                    new Thread(()->{
                        HashMap<String, Object> from = new HashMap<>();
                        Intent intent = getIntent();
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.set("userId",Myconfig.getValue("user.id"));
                        jsonObject.set("content",content);
                        from.put("room_id",intent.getIntExtra("room_id",1));
                        from.put("user_id",Myconfig.getValue("user.id"));
                        from.put("msg",jsonObject.toString());
                        String body = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/message/send").form(from).execute().body();
                        ws.send(jsonObject.toString());
                        runOnUiThread(()->{
                            Integer data1 = new JSONObject(body).getInt("code");
                            if (data1.equals(200)){
//                                Toast.makeText(TalkActivity.this,"发送成功",Toast.LENGTH_SHORT).show();
                                TalkBean msg = new TalkBean(content, TalkBean.TYPE_SEND,"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png","https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png");
                                msgList.add(msg);
                                adapter.notifyDataSetChanged();
                                msgListView.setSelection(msgList.size());
                                inputText.setText("");
                            }
                        });
                    }).start();

                }
            }
        });
        ws = HTTP.builder().build().webSocket(Const.PUBLIC_SERVER.replace("http", "ws") + "/api/pushMessage/" + getIntent().getIntExtra("room_id", 0) + "/apctoken")
                .setOnOpen(new WebSocket.Listener<HttpResult>() {
                    @Override
                    public void on(WebSocket ws, HttpResult data) {
                        System.out.println("ok");
                    }
                }).setOnMessage(new WebSocket.Listener<WebSocket.Message>() {
                    @Override
                    public void on(WebSocket ws, WebSocket.Message data) {
                        String body = new String(data.toBytes());
                        JSONObject jsonObject = new JSONObject(body);
                        //String res=new JSONObject(body).getStr("content");
                        runOnUiThread(()->{
                            TalkBean msg1 = new TalkBean(jsonObject.getStr("msg"), TalkBean.TYPE_RECEIVED,"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png",Myconfig.getValue("user.headpic"));
                            msgList.add(msg1);
                            msgListView.setSelection(msgList.size());
                        });
                    }
                }).listen();

    }

    private void initMsgs() {
        //gethistory
//        TalkBean msg1 = new TalkBean("Hello, how are you?", TalkBean.TYPE_RECEIVED,"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png","https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png");
//        msgList.add(msg1);
//        TalkBean msg2 = new TalkBean("Fine, thank you, and you?", TalkBean.TYPE_SEND,"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png","https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png");
//        msgList.add(msg2);
        new Thread(()->{

            HashMap<String, Object> from = new HashMap<>();
            Intent intent = getIntent();
            from.put("channel",intent.getIntExtra("room_id",1));
            if (Myconfig.getValue("user.id").length()==0){
                startActivity(new Intent(TalkActivity.this, MainActivity.class));
            }
            from.put("userid",Myconfig.getValue("user.id"));

            String body = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/message/getList").form(from).execute().body();
            JSONObject jsonObject=new JSONObject(body);
            JSONArray node=jsonObject.getJSONArray("data");

            from.put("shopid",intent.getIntExtra("room_id",1));
            String body1 = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/shop/queryByshopid").form(from).execute().body();
            JSONObject jsonObject1=new JSONObject(body1);

            for (int i=0;i<jsonObject.getJSONArray("data").size();i++){
                int type = 0;
                if(node.getJSONObject(i).getInt("uid").equals(node.getJSONObject(i).getJSONObject("msg").getInt("userId")))type=1;
                msgList.add(new TalkBean(node.getJSONObject(i).getJSONObject("msg").getStr("content"), type, jsonObject1.getJSONObject("data").getStr("shopUrl"), Myconfig.getValue("user.headpic")));
            }
            runOnUiThread(()->{
                msgListView.setSelection(msgList.size());
            });

        }).start();
    }
}