package com.llw.changeavatar.Activity.shop.activity;

import static com.llw.changeavatar.Activity.ButtomTab.activityList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.TimePickerView;
import com.llw.changeavatar.Activity.ButtomTab;
import com.llw.changeavatar.Activity.shop.StoreDetailsLifeActivity;
import com.llw.changeavatar.Activity.shop.activity.entry.ProductListEntity;
import com.llw.changeavatar.Activity.shop.activity.entry.ShopCart;
import com.llw.changeavatar.Adapter.PurchaseListAdapter;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.PurchaseListBean;
import com.llw.changeavatar.utils.Const;
import com.llw.changeavatar.utils.Myconfig;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;

public class PayingActivity extends AppCompatActivity {
    TimePickerView pvTime;
    Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityList.add(this);
        ShopCart data =(ShopCart)getIntent().getSerializableExtra("data");
        for (Map.Entry<ProductListEntity.ProductEntity, Integer> productEntityIntegerEntry : data.getShoppingSingle().entrySet()) {
            System.out.println("------------"+productEntityIntegerEntry.getKey());
            System.out.println("------------"+productEntityIntegerEntry.getValue());
        }
        setContentView(R.layout.paying_now);
        ImageView back=findViewById(R.id.iv_back);
        back.setOnClickListener(view -> finish());

        Intent intent = getIntent();
        TextView order_list_comment= findViewById(R.id.order_list_comment_paying);
        TextView detail_list_toolbar= findViewById(R.id.detail_list_toolbar_paying);
        TextView order_list_price_paying =findViewById(R.id.order_list_price_paying);
        order_list_comment.setText("立即付款");
        JSONArray objects = new JSONArray();

        RecyclerView viewById = (RecyclerView) findViewById(R.id.detail_recycler1_paying);
        mContext = this;
        List<PurchaseListBean.DataBean> list = new ArrayList<>();
        for (Map.Entry<ProductListEntity.ProductEntity, Integer> productEntityIntegerEntry : data.getShoppingSingle().entrySet()) {
            list.add(new PurchaseListBean.DataBean(productEntityIntegerEntry.getKey().getProductImg(),productEntityIntegerEntry.getKey().getProductName(),productEntityIntegerEntry.getKey().toString(),productEntityIntegerEntry.getValue(),(int) Math.round(productEntityIntegerEntry.getKey().getProductMoney())*productEntityIntegerEntry.getValue()));
            JSONObject jsonObject = new JSONObject();
            System.out.println(productEntityIntegerEntry.getKey());
            objects.put(new JSONObject().set("id",productEntityIntegerEntry.getKey().getProductId()).set("count",productEntityIntegerEntry.getValue()));
        }

        TextView day_select = findViewById(R.id.day_select1);
        day_select .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //点击组件的点击事件
                if (pvTime!=null){
                    pvTime.show(day_select);
                }
            }
        });
        Calendar selectedDate = Calendar.getInstance();
        Calendar startDate = Calendar.getInstance();
        int year = startDate.get(Calendar.YEAR);
        int month = startDate.get(Calendar.MONTH);
        int day = startDate.get(Calendar.DAY_OF_MONTH);
        int hour = startDate.get(Calendar.HOUR_OF_DAY);
        int minute = startDate.get(Calendar.MINUTE)+1;
        startDate.set(year, month, day, hour, minute);
        Calendar endDate = Calendar.getInstance();
        endDate.set(year, 12, 31,23,59);
        //时间选择器
        pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                // 这里回调过来的v,就是show()方法里面所添加的 View 参数，如果show的时候没有添加参数，v则为null
                TextView btn = (TextView) v;
                Date date1 = new Date();
                if( date1.compareTo(date)<1){
                    btn.setText(getTimes1(date));
                }else {
                    Toast.makeText(mContext,"日期不对",Toast.LENGTH_SHORT).show();
                    btn.setText("");
                }

            }
        })
                .setTitleText("预定时间")
                .setType(new boolean[]{false, true, true, true, true, false})
                .setLabel("年", "月", "日", "时", "分", "秒")
                .isCenterLabel(true)
                .setDividerColor(Color.DKGRAY)
                .setContentSize(16)//字号
                .setDate(selectedDate)
                .setRangDate(startDate, endDate)
                .setDecorView(null)
                .build();



        order_list_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Myconfig.getValue("user.id").length()==0){
                    System.exit(0);
                }
                HashMap<String, Object> body = new HashMap<>();
                //TODO
                body.put("shopid",getIntent().getIntExtra("shop_id",0));
                body.put("userid", Myconfig.getValue("user.id"));
                body.put("time",System.currentTimeMillis()-24*60*60*1000*1+"");
                body.put("orderItems", objects.toString());
                new Thread(()->{
                    String body1 = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/order/createOrder").form(body).execute().body();
                    System.out.println(body1);
                }).start();
                Intent i = new Intent(PayingActivity.this, ButtomTab.class);
                startActivity(i);
            }

        });
        order_list_price_paying.setText("$"+data.getShoppingTotalPrice());
        PurchaseListAdapter adapter = new PurchaseListAdapter(mContext, list);
        viewById.setLayoutManager(new LinearLayoutManager(mContext));
        viewById.setAdapter(adapter);
    }
    private String getTimes1(Date date) {//年月日时分秒格式
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return format.format(date);
    }
}