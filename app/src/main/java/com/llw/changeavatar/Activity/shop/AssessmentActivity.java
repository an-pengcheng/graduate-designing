package com.llw.changeavatar.Activity.shop;

import static com.llw.changeavatar.Activity.ButtomTab.activityList;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.llw.changeavatar.Adapter.AssListAdapter;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.AssListBean;
import com.llw.changeavatar.utils.Const;
import com.llw.changeavatar.utils.Myconfig;

import java.util.HashMap;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;

public class AssessmentActivity extends AppCompatActivity {

    private RatingBar rb_normal1;
    private RatingBar rb_normal2;
    private RatingBar rb_normal3;
    private CheckBox anonymous;
    private Button assessment_button;
    private float start1,start2,start3;
    private boolean is_anonymous;
    private EditText assessment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.assessment_now);
        activityList.add(this);
        assessment_button=findViewById(R.id.button_assessment);
        assessment=findViewById(R.id.assessment);
        rb_normal1=  findViewById(R.id.ratingBar);
        rb_normal2=  findViewById(R.id.ratingBar2);
        rb_normal3=  findViewById(R.id.ratingBar3);
        anonymous=findViewById(R.id.checkBox_is_anonymous);
        rb_normal1.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                start1=rating;
            }
        });
        rb_normal2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                start2=rating;
            }
        });
        rb_normal3.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> start3=rating);
        assessment_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String assess= assessment.getText().toString().trim();
                if(anonymous.isChecked()){
                    is_anonymous=true;
                }else {
                    is_anonymous=false;
                }
                if(assessment.length()>0){
                    new Thread(()->{
                        HashMap<String, Object> from = new HashMap<>();
                        Intent intent = getIntent();
                        from.put("shopid",intent.getIntExtra("shop_id",0));
                        from.put("user_id", Myconfig.getValue("user.id"));
                        from.put("orderid",intent.getIntExtra("id",1));
                        from.put("star",(int)(start1*2));
                        from.put("context",assess);
                        String body = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/evaluation/submit").form(from).execute().body();
                        runOnUiThread(()->{
                            Integer data1 = new JSONObject(body).getInt("code");
                            if (data1==200){
                                Toast.makeText(AssessmentActivity.this, "评价成功",
                                        Toast.LENGTH_LONG).show();
                            }
                        });
                    }).start();
                }else {
                    Toast.makeText(AssessmentActivity.this, "评价失败,评价内容不能为空",
                            Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}

