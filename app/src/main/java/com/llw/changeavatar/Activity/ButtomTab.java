package com.llw.changeavatar.Activity;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.llw.changeavatar.Activity.shop.AssessmentActivity;
import com.llw.changeavatar.Activity.shop.OrderActivity;
import com.llw.changeavatar.Adapter.LoopViewAdapter;
import com.llw.changeavatar.Fragment.DataGenerator;
import com.llw.changeavatar.Fragment.HomePageFragment;
import com.llw.changeavatar.Fragment.MassagePageFragment;
import com.llw.changeavatar.Fragment.ShopPageFragment;
import com.llw.changeavatar.R;
import com.llw.changeavatar.pagerOnClickListener;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ButtomTab extends AppCompatActivity {
    private TabLayout tab_layout;
    private Fragment[] framensts;
    public static List<Activity> activityList = new LinkedList();


    /**
     * 开启View闪烁效果
     *
     * */
    private void startFlick( View view ){
        if( null == view ){
            return;
        }
        Animation alphaAnimation = new AlphaAnimation( 1, 0 );
        alphaAnimation.setDuration( 1000 );
        alphaAnimation.setInterpolator( new LinearInterpolator( ) );
        alphaAnimation.setRepeatCount( Animation.INFINITE );
        alphaAnimation.setRepeatMode( Animation.REVERSE );
        view.startAnimation( alphaAnimation );
    }

    /**
     * 取消View闪烁效果
     *
     * */
    private void stopFlick( View view ){
        if( null == view ){
            return;
        }
        view.clearAnimation();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.button_tab);
        activityList.add(this);
        int id1 = getIntent().getIntExtra("id", 0);
        if (id1 == 1) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.sign_in,new HomePageFragment())
                    .addToBackStack(null)
                    .commit();
        }
        framensts = DataGenerator.getFragments();
        initView();
        int id = getIntent().getIntExtra("id",2);
        if(id == 2) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.order_list_comment, new MassagePageFragment())
                    .addToBackStack(null);
        }

//        FloatingActionButton massage_button=(FloatingActionButton) findViewById(R.id.fab);
//        startFlick(massage_button);
//        massage_button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String myChannelId = "myChannel_1";
//                String myChannelName = "myChannel";
//
//                NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//                Notification notification = null;
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    NotificationChannel channel = new NotificationChannel(myChannelId, myChannelName,
//                            NotificationManager.IMPORTANCE_LOW);
//                    manager.createNotificationChannel(channel);
//                    notification = new NotificationCompat.Builder(view.getContext(), myChannelId)
//                            .setContentTitle("消息通知")
//                            .setContentText("你有一个新的店铺消息")
//                            .setWhen(System.currentTimeMillis())
//                            .setShowWhen(true)
//                            .setSmallIcon(R.drawable.shopping)
//                            .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.icons8_grocery_bag_1))
//                            .build();
//                }
//                manager.notify(1, notification);
//                stopFlick(massage_button);
//
//                startActivity(new Intent(ButtomTab.this,TalkActivity.class));
//            }
//        });
    }


    private void initView() {
        tab_layout = (TabLayout) findViewById(R.id.bottom_tab_layout);

        //设置监听器
        tab_layout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                onTabItemSelected(tab.getPosition());

                // Tab 选中之后，改变各个Tab的状态
                for (int i = 0; i < tab_layout.getTabCount(); i++) {
                    View view = tab_layout.getTabAt(i).getCustomView();
                    ImageView icon = (ImageView) view.findViewById(R.id.tab_content_image);
                    TextView text = (TextView) view.findViewById(R.id.tab_content_text);

                    if (i == tab.getPosition()) { // 选中状态，修改字体颜色和图片，背景未实现
//                        view.setBackgroundColor(Color.parseColor("#a9a9a9"));
                        icon.setImageResource(DataGenerator.mTabResPressed[i]);
                        text.setTextColor(getResources().getColor(android.R.color.black));
                    } else {// 未选中状态
//                        view.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        icon.setImageResource(DataGenerator.mTabRes[i]);
                        text.setTextColor(getResources().getColor(android.R.color.darker_gray));
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        // 提供自定义的布局添加Tab，注意此处的加载页面需要在设置Listener之后，不然会导致第一次点击事件失效
        for(int i=0;i<4;i++){
            tab_layout.addTab(tab_layout.newTab().setCustomView(DataGenerator.getTabView(this,i)));
        }

        //默认进入首页（需放在加载页面之后）
        tab_layout.getTabAt(0).select();
    }

    private void onTabItemSelected(int position){
        Fragment frag = null;
        switch (position){
            case 0:
                frag = framensts[0];
                break;
            case 1:
                frag = framensts[1];
                break;

            case 2:
                frag = framensts[2];
                break;
            case 3:
                frag = framensts[3];
                break;
        }
        //替换fragment
        if(frag!=null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.home_container,frag).commit();
        }
    }
}
