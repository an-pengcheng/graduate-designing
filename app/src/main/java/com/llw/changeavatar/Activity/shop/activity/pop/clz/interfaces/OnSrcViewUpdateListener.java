package com.llw.changeavatar.Activity.shop.activity.pop.clz.interfaces;

import androidx.annotation.NonNull;

import com.llw.changeavatar.Activity.shop.activity.pop.clz.core.ImageViewerPopupView;

/**
 * Description:
 * Create by dance, at 2019/1/29
 */
public interface OnSrcViewUpdateListener {
    void onSrcViewUpdate(@NonNull ImageViewerPopupView popupView, int position);
}
