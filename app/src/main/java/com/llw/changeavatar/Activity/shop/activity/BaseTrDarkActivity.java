package com.llw.changeavatar.Activity.shop.activity;

import static com.llw.changeavatar.Activity.ButtomTab.activityList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


import com.llw.changeavatar.R;
import com.llw.changeavatar.utils.StatusBarUtil;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @ClassName: BaseActivity
 * @Description:Activity基类-状态栏透明深色字体，可以自己设置状态栏颜色
 * @Author: dingchao
 * @Date: 2020/6/1 11:13
 */
public abstract class BaseTrDarkActivity extends AppCompatActivity {
    protected Context mContext;
    protected Activity mActivity;
    protected Unbinder mUnbinder;

    protected boolean isRegistered = false;

    /**
     * 视图绑定
     *
     * @param layoutResId
     */
    @Override
    public void setContentView(int layoutResId) {
        super.setContentView(layoutResId);
    }

//    @Override
//    public void setContentView(View view) {
//        super.setContentView(view);
//    }


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //设置禁止横屏
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        activityList.add(this);
        mContext = this;
        mActivity = this;

        //设置无标题
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(getLayout());

        //ButterKnife绑定,注意要在绑定试图之后写
        mUnbinder = ButterKnife.bind(this);
        savedInstanceState(savedInstanceState);
        initEvent();

        //用来设置整体下移，状态栏沉浸
        StatusBarUtil.setRootViewFitsSystemWindows(this, false);
        StatusBarUtil.transparencyBar(this);
        StatusBarUtil.setImmersiveStatusBar(this,true,
                getResources().getColor(R.color.color_00000000));

        //网络监听相关
        //注册网络状态监听广播
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        isRegistered = true;
        initListener();
        initData();

        Intent intent = getIntent();
        ((TextView)findViewById(R.id.my_shop_name)).setText(intent.getStringExtra("title"));
        ((TextView)findViewById(R.id.my_tv_app_title_text)).setText(intent.getStringExtra("title"));
        ((TextView)findViewById(R.id.my_shop_new_food_time)).setText("主营："+intent.getStringExtra("salemain"));
        ((TextView)findViewById(R.id.salecount)).setText(intent.getStringExtra("salecount")+"单");
        if(intent.getStringExtra("salecount")==null) ((TextView)findViewById(R.id.salecount)).setText("0 单");
        Picasso.get().load(intent.getStringExtra("url")).into((ImageView) findViewById(R.id.my_store_details_top_img_1));
        Picasso.get().load(intent.getStringExtra("url")).into((ImageView) findViewById(R.id.my_iv_header));


    }


    @Override
    protected void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        //网络监听解绑
        super.onDestroy();
    }

    protected abstract int getLayout();

    protected abstract void initEvent();

    protected abstract void noNetWork();
    protected abstract void initListener();

    protected abstract void initData();
    protected abstract void savedInstanceState(Bundle savedInstanceState);
}
