package com.llw.changeavatar.Activity.Login;

import static com.llw.changeavatar.Activity.ButtomTab.activityList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.llw.changeavatar.Adapter.AssListAdapter;
import com.llw.changeavatar.Adapter.MassageListAdapter;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.AssListBean;
import com.llw.changeavatar.bean.MassageListBean;
import com.llw.changeavatar.utils.Const;
import com.llw.changeavatar.utils.Myconfig;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.OnClick;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;

public class MassageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_massage);
        activityList.add(this);
        RecyclerView viewById = (RecyclerView) findViewById(R.id.home_recycler_massage);
        Context mContext = this;
        ImageView back=findViewById(R.id.iv_back);
        back.setOnClickListener(view -> finish());
        new Thread(()->{
            HashMap<String, Object> from = new HashMap<>();
            Myconfig.init(this);
            if(Myconfig.getValue("user.id").length()==0){
                startActivity(new Intent(MassageActivity.this, MainActivity.class));
            }
            from.put("user_id",Myconfig.getValue("user.id"));
            String body = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/message/getALL").form(from).execute().body();
            runOnUiThread(()->{
                List<MassageListBean.DataBean> list = new ArrayList<>();
                JSONObject data1 = new JSONObject(body).getJSONObject("data");
                for (String item :   data1.keySet()) {
                    JSONArray jsonObject =data1.getJSONArray(item);
                    JSONObject jsonObject1 = jsonObject.getJSONObject(jsonObject.size() - 1);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
                    String formatStr2 =formatter.format(new Date(jsonObject1.getLong("createdate")));
                    list.add(new MassageListBean.DataBean(formatStr2,"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png",jsonObject1.getJSONObject("msg").getStr("content"),"1","PPPP",jsonObject1.getInt("channel"),jsonObject1.getInt("uid")));
                }
                list.add(new MassageListBean.DataBean(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").
                                format(new Date()).toString(),"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png",
                                "质量评价","1","系统消息",0,0));
                MassageListAdapter adapter = new MassageListAdapter(mContext, list);
                viewById.setLayoutManager(new LinearLayoutManager(mContext));
                viewById.setAdapter(adapter);
            });
        }).start();
    }
}