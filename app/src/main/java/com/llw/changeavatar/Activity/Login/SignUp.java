package com.llw.changeavatar.Activity.Login;


import static com.llw.changeavatar.Activity.ButtomTab.activityList;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.llw.changeavatar.R;
import com.llw.changeavatar.utils.Const;
import com.xuexiang.xui.XUI;

import java.util.HashMap;
import java.util.Map;

import cn.hutool.core.map.MapUtil;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

public class SignUp extends AppCompatActivity {
    private static final String TAG = "SignUp";

    EditText username_sign_up;
    EditText userpwd_sign_up;
    EditText userverification;
    Button button_sign_in;
    ImageView imView;
    String Sid;

    protected void onCreate(Bundle savedInstanceState) {
        XUI.initTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);
        activityList.add(this);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        TextView btn =  findViewById(R.id.head_pic);
        btn .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SignUp.this , UserInfo.class);
                startActivity(i);
            }
        });
        username_sign_up = findViewById(R.id.username_sign_up);
        userpwd_sign_up = findViewById(R.id.userpwd_sign_up);
        userverification =findViewById(R.id.useverification);
        button_sign_in= findViewById(R.id.sign_up_true);
        button_sign_in.setOnClickListener(corkListener);
        imView = (ImageView) findViewById(R.id.identifyingcode_image);
        gitPictureCode();
        imView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gitPictureCode();
            }
        });
    }

    private void gitPictureCode() {
        HttpResponse httpResponse=HttpUtil.createGet(Const.PUBLIC_SERVER+"/api/getcaptcha").execute();
        byte[] bodyBytes=httpResponse.bodyBytes();
        Bitmap bitmap=BitmapFactory.decodeStream(httpResponse.bodyStream());
        imView.setImageBitmap(bitmap);
        Sid=httpResponse.header("sid");
        System.out.println(Sid);
    }
    private View.OnClickListener picListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            gitPictureCode();
        }
    };
    private View.OnClickListener corkListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String user = username_sign_up.getText().toString().trim();
            String pwd = userpwd_sign_up.getText().toString().trim();
            String code = userverification.getText().toString().trim();

            if (code !=null&& !"".equals(code)) {
                Map<String, String> map = MapUtil.builder(new HashMap<String, String>()).
                        put("username", user).
                        put("userpwd", pwd).
                        put("code", code).
                        put("sid", Sid).
                        build();
                String res = HttpUtil.createPost(Const.PUBLIC_SERVER+"/api/user/sturegister").formStr(map).execute().body();
                JSONObject jsonObject = JSONUtil.parseObj(res);
                jsonObject.getShort("code");
                Log.d(TAG, "onClick: "+JSONUtil.parseObj(res));
                Log.d(TAG, "onClick: "+jsonObject.getInt("code"));
                Intent intent = new Intent(SignUp.this , MainActivity.class);
                startActivity(intent);
            }else{
                Toast.makeText(getApplicationContext(), "用户名不能为空！", Toast.LENGTH_SHORT).show();
            }
        }
    };

}

