package com.llw.changeavatar.Activity.shop.activity.imp;

import android.view.View;
import com.llw.changeavatar.Activity.shop.activity.entry.ProductListEntity;

/**
 *
 */
public interface ShopCartImp {
    void add(View view, int postion, ProductListEntity.ProductEntity entity);

    void remove(View view, int postion, ProductListEntity.ProductEntity entity);
}
