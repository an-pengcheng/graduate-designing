package com.llw.changeavatar.Activity.Login;

import static com.llw.changeavatar.Activity.ButtomTab.activityList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.llw.changeavatar.Activity.ButtomTab;
import com.llw.changeavatar.Adapter.AssListAdapter;
import com.llw.changeavatar.Adapter.FavListAdapter;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.AssListBean;
import com.llw.changeavatar.bean.FavoriteListBean;
import com.llw.changeavatar.utils.Const;
import com.llw.changeavatar.utils.Myconfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;

public class FavoritesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        activityList.add(this);
        RecyclerView viewById = (RecyclerView) findViewById(R.id.home_recycler_favorites);
        Context mContext = this;
        ImageView back=findViewById(R.id.iv_back);
        back.setOnClickListener(view -> finish());
        List<FavoriteListBean.DataBean> list = new ArrayList<>();
        new Thread(()->{
            HashMap<String, Object> from = new HashMap<>();
            if(Myconfig.getValue("user.id").length()==0){
                startActivity(new Intent(FavoritesActivity.this, MainActivity.class));
            }
            from.put("user_id", Myconfig.getValue("user.id"));
            String body = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/fav/queryByuser").form(from).execute().body();
            runOnUiThread(()->{
                JSONArray data1 = new JSONObject(body).getJSONArray("data");

                for (Object item : data1) {
                    JSONObject jsonObject = new JSONObject(item);
                    list.add(new FavoriteListBean.DataBean(jsonObject.getInt("shop_id"),jsonObject.getInt("user_id"),
                            jsonObject.getJSONObject("shopinfo").getStr("shopUrl"),
                            jsonObject.getJSONObject("shopinfo").getStr("shopName"),
                            jsonObject.getJSONObject("shopinfo").getStr("shopAvgTime"),
                            jsonObject.getJSONObject("shopinfo").getStr("shopCate"),
                            jsonObject.getJSONObject("shopinfo").getStr("shopRemark")));
                }
                FavListAdapter adapter = new FavListAdapter(mContext, list);
                viewById.setLayoutManager(new LinearLayoutManager(mContext));
                viewById.setAdapter(adapter);
            });
        }).start();
    }
}