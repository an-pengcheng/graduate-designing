package com.llw.changeavatar.Activity.Login;

import static com.llw.changeavatar.Activity.ButtomTab.activityList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.imageview.ShapeableImageView;
import com.llw.changeavatar.Activity.ButtomTab;
import com.llw.changeavatar.R;
import com.llw.changeavatar.utils.Myconfig;
import com.squareup.picasso.Picasso;

public class UserInfo extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_info);
        activityList.add(this);
        Button btn = findViewById(R.id.change_info);
        ImageView back=findViewById(R.id.iv_back);
        back.setOnClickListener(view -> finish());
        TextView yonghuxingming=findViewById(R.id.yonghuxingming);
        TextView tv_fxid=findViewById(R.id.tv_fxid);
        ShapeableImageView viewById = (ShapeableImageView)findViewById(R.id.iv_img_change);
        Myconfig.init(this);
        if(Myconfig.getValue("user.headpic").length()>0){
            Picasso.get().load(Myconfig.getValue("user.headpic")).into(viewById);
        }else{
            Picasso.get().load("https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png").into(viewById);
        }
        if(Myconfig.getValue("user.nickname").length()>0){
            yonghuxingming.setText(Myconfig.getValue("user.nickname"));
        }else {
            yonghuxingming.setText("请先登录");
        }
        if(Myconfig.getValue("user.username").length()>0){
            tv_fxid.setText("账号："+Myconfig.getValue("user.username"));
        }else{
            tv_fxid.setText("账号：");
        }
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(UserInfo.this, UserInfoChange.class);
                startActivity(i);
            } 
        });
        Button mainPage= (Button) findViewById(R.id.outline1);
        mainPage .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Myconfig.delateAll();
                exit();
            }
        });
    }

    public void exit(){
        for(Activity act:activityList){
            act.finish();
        }
        System.exit(0);
    }
}
