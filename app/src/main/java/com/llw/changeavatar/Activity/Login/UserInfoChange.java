package com.llw.changeavatar.Activity.Login;


import static com.llw.changeavatar.Activity.ButtomTab.activityList;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.imageview.ShapeableImageView;
import com.llw.changeavatar.R;
import com.llw.changeavatar.utils.BitmapUtils;
import com.llw.changeavatar.utils.CameraUtils;
import com.llw.changeavatar.utils.Const;
import com.llw.changeavatar.utils.Myconfig;
import com.llw.changeavatar.utils.SPUtils;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.xuexiang.xui.XUI;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cn.hutool.core.map.MapUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;

/**
 * 更换用户头像
 *
 * @author llw
 */
public class UserInfoChange extends AppCompatActivity {
    String pic;
    File filePic;
    Button button_change;
    ImageView button_back;

    //权限请求
    private RxPermissions rxPermissions;

    //是否拥有权限
    private boolean hasPermissions = false;

    //底部弹窗
    private BottomSheetDialog bottomSheetDialog;
    //弹窗视图
    private View bottomView;

    //存储拍完照后的图片
    private File outputImagePath;
    //启动相机标识
    public static final int TAKE_PHOTO = 1;
    //启动相册标识
    public static final int SELECT_PHOTO = 2;

    //图片控件
    private ShapeableImageView ivHead;
    //Base64
    private String base64Pic;
    //拍照和相册获取图片的Bitmap
    private Bitmap orc_bitmap;

    //Glide请求图片选项配置
    private RequestOptions requestOptions = RequestOptions.circleCropTransform()
            .diskCacheStrategy(DiskCacheStrategy.NONE)//不做磁盘缓存
            .skipMemoryCache(true);//不做内存缓存

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        XUI.initTheme(this);
        Context context=this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_info_change);
        activityList.add(this);
        ivHead = findViewById(R.id.iv_avatar);
        button_back= findViewById(R.id.iv_back);
        button_back.setOnClickListener(view -> finish());
        button_change= findViewById(R.id.save_change);
        EditText  tv_name=findViewById(R.id.tv_name);
        ShapeableImageView viewById = (ShapeableImageView)findViewById(R.id.iv_avatar);
        if(Myconfig.getValue("user.haedpic").length()>0){
            Picasso.get().load(Myconfig.getValue("user.haedpic")).into(viewById);
        }else{
            Picasso.get().load("https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png").into(viewById);
        }
        if(Myconfig.getValue("user.nickname").length()>0){
            tv_name.setText(Myconfig.getValue("user.nickname"));
        }else {
            tv_name.setText("请先登录");
        }

        button_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new Thread(()-> {
                    Map<String, String> map = MapUtil.builder(new HashMap<String, String>()).
                            put("username", Myconfig.getValue("user.username")).
                            put("userpwd", Myconfig.getValue("user.userpwd")).build();
                    String res1 = HttpUtil.createPost( Const.PUBLIC_SERVER+"/api/user/login").formStr(map).execute().body();
                    System.out.println(res1);
                    pic = HttpUtil.createPost("https://demo111.gzxtest.xyz/upload").form("file", filePic).execute().body();
                    JSONObject jsonObject = new JSONObject(pic);
                    System.out.println(pic);
                    String smsgs = "https://demo111.gzxtest.xyz/"+jsonObject.getStr("msg");
                    String res = HttpUtil.createPost( Const.PUBLIC_SERVER+"/api/user/updateinfo").form("headpic",smsgs).execute().body();
                    res=HttpUtil.createPost( Const.PUBLIC_SERVER+"/api/user/login").formStr(map).execute().body();
                    Myconfig.SetValue("user.username",new JSONObject(res).getJSONObject("data").getJSONObject("userinfo").getStr("username"));
                    Myconfig.SetValue("user.headpic",new JSONObject(res).getJSONObject("data").getJSONObject("userinfo").getStr("headpic"));

                    System.out.println(res);
                    runOnUiThread(()->{
                        Toast.makeText(context, "修改成功", Toast.LENGTH_SHORT).show();
                    });

                    System.out.println(Myconfig.getValue("user.headpic"));
                }).start();
            }
        });


        //检查版本
        checkVersion();
        //取出缓存
        String imageUrl = SPUtils.getString("imageUrl", null, this);
        if (imageUrl != null) {
            Glide.with(this).load(imageUrl).apply(requestOptions).into(ivHead);
        }
    }
    public View.OnClickListener mGoBack = new View.OnClickListener() {

        public void onClick(View v) {
        }
    };

    /**
     * 检查版本
     */
    private void checkVersion() {
        //Android6.0及以上版本
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //如果你是在Fragment中，则把this换成getActivity()
            rxPermissions = new RxPermissions(this);
            //权限请求
            rxPermissions.request(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .subscribe(granted -> {
                        if (granted) {//申请成功
                            showMsg("已获取权限");
                            hasPermissions = true;
                        } else {//申请失败
                            showMsg("权限未开启");
                            hasPermissions = false;
                        }
                    });
        } else {
            //Android6.0以下
            showMsg("无需请求动态权限");
        }
    }

    /**
     * 更换头像
     *
     * @param view
     */
    public void changeAvatar(View view) {
        bottomSheetDialog = new BottomSheetDialog(this);
        bottomView = getLayoutInflater().inflate(R.layout.dialog_bottom, null);
        bottomSheetDialog.setContentView(bottomView);
//        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundColor(Color.TRANSPARENT);
        TextView tvTakePictures = bottomView.findViewById(R.id.tv_take_pictures);
        TextView tvOpenAlbum = bottomView.findViewById(R.id.tv_open_album);
        TextView tvCancel = bottomView.findViewById(R.id.tv_cancel);

        //拍照
        tvTakePictures.setOnClickListener(v -> {
            takePhoto();
            showMsg("拍照");
            bottomSheetDialog.cancel();
        });
        //打开相册
        tvOpenAlbum.setOnClickListener(v -> {
            openAlbum();
            showMsg("打开相册");
            bottomSheetDialog.cancel();
        });
        //取消
        tvCancel.setOnClickListener(v -> {
            bottomSheetDialog.cancel();
        });
        //底部弹窗显示
        bottomSheetDialog.show();
    }

    /**
     * 拍照
     */
    private void takePhoto() {
        if (!hasPermissions) {
            showMsg("未获取到权限");
            checkVersion();
            return;
        }
        SimpleDateFormat timeStampFormat = new SimpleDateFormat(
                "yyyy_MM_dd_HH_mm_ss");
        String filename = timeStampFormat.format(new Date());
        outputImagePath = new File(getExternalCacheDir(),
                filename + ".jpg");
        Intent takePhotoIntent = CameraUtils.getTakePhotoIntent(this, outputImagePath);
        // 开启一个带有返回值的Activity，请求码为TAKE_PHOTO
        startActivityForResult(takePhotoIntent, TAKE_PHOTO);
    }

    /**
     * 打开相册
     */
    private void openAlbum() {
        if (!hasPermissions) {
            showMsg("未获取到权限");
            checkVersion();
            return;
        }
        startActivityForResult(CameraUtils.getSelectPhotoIntent(), SELECT_PHOTO);
    }

    /**
     * 返回到Activity
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            //拍照后返回
            case TAKE_PHOTO:
                if (resultCode == RESULT_OK) {
                    //显示图片
                    displayImage(outputImagePath.getAbsolutePath());

                    filePic= new File(outputImagePath.getAbsolutePath());

                }
                break;
            //打开相册后返回
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
                    String imagePath = null;
                    //判断手机系统版本号
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                        //4.4及以上系统使用这个方法处理图片
                        imagePath = CameraUtils.getImageOnKitKatPath(data, this);
                    } else {
                        imagePath = CameraUtils.getImageBeforeKitKatPath(data, this);
                    }
                    //显示图片
                    displayImage(imagePath);

                    filePic= new File(imagePath);

                }
                break;
            default:
                break;
        }
    }

    /**
     * 通过图片路径显示图片
     */
    private void displayImage(String imagePath) {
        if (!TextUtils.isEmpty(imagePath)) {

            //放入缓存
            SPUtils.putString("imageUrl", imagePath, this);

            //显示图片
            Glide.with(this).load(imagePath).apply(requestOptions).into(ivHead);

            //压缩图片
            orc_bitmap = CameraUtils.compression(BitmapFactory.decodeFile(imagePath));
            //转Base64
            base64Pic = BitmapUtils.bitmapToBase64(orc_bitmap);

        } else {
            showMsg("图片获取失败");
        }
    }


    /**
     * Toast提示
     *
     * @param msg
     */
    private void showMsg(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


}
