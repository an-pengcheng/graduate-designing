package com.llw.changeavatar.Activity.Login;


import static com.llw.changeavatar.Activity.ButtomTab.activityList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.llw.changeavatar.Activity.ButtomTab;
import com.llw.changeavatar.R;
import com.llw.changeavatar.utils.Const;
import com.llw.changeavatar.utils.CustomDialog;
import com.llw.changeavatar.utils.Myconfig;
import com.xuexiang.xui.XUI;

import java.util.HashMap;
import java.util.Map;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = "SignUp";

    EditText username;
    EditText userpwd;
    private CustomDialog customDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        XUI.initTheme(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activityList.add(this);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        Button btn = findViewById(R.id.sign_up);
        btn .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this , SignUp.class);
                startActivity(i);
            }
        });

        username = findViewById(R.id.username);
        userpwd = findViewById(R.id.userpwd);
        Button button_sign_in= findViewById(R.id.sign_in);
        button_sign_in.setOnClickListener(corkyListener);
        if(autoLogin()){
            //TODO : 跳转
        }
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        //注入字体
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
    private View.OnClickListener corkyListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String user = username.getText().toString().trim();
            String pwd = userpwd.getText().toString().trim();
            Map<String, String> map = MapUtil.builder(new HashMap<String, String>()).
                    put("username", user).
                    put("userpwd", pwd).build();
            JSONObject jsonObject = JSONUtil.parseObj(HttpUtil.createPost( Const.PUBLIC_SERVER +"/api/user/login").formStr(map).execute().body());
            System.out.println(jsonObject);
            if (jsonObject.getInt("code")==200) {
                JSONObject data = jsonObject.getJSONObject("data");
                Myconfig.SetValue("tokenName",data.getJSONObject("token").getStr("tokenName"));
                Myconfig.SetValue("token",data.getJSONObject("token").getStr("token"));
                Myconfig.SetValue("user.id",data.getJSONObject("userinfo").getInt("id").toString());
                Myconfig.SetValue("user.username",data.getJSONObject("userinfo").getStr("username"));
                Myconfig.SetValue("user.headpic",data.getJSONObject("userinfo").getStr("headpic"));
                Myconfig.SetValue("user.userpwd",pwd);
                Myconfig.SetValue("user.nickname",data.getJSONObject("userinfo").getStr("nickname"));
                Toast.makeText(getApplicationContext(), "欢迎你新来的美食家", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, ButtomTab.class);
                intent.putExtra("id", Myconfig.getValue("user.id"));
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "验证错误，禁止访问", Toast.LENGTH_SHORT).show();
            }
        }
    };
    public void show(View view) {
        customDialog = new CustomDialog(this);
        customDialog.show();
    }
    private boolean autoLogin() {
        Myconfig.init(MainActivity.this);
        String tokenName = Myconfig.getValue("tokenName");
        HashMap<String, String> cookie = new HashMap<>(2);
        if (!StrUtil.isBlank(tokenName)) {
            cookie.put("Cookie", tokenName + "=" + Myconfig.getValue("token"));
        }
        if (cookie.size() > 0) {
            JSONObject jsonObject = JSONUtil.parseObj(HttpUtil.createGet(Const.PUBLIC_SERVER + "/api/user/islogin").addHeaders(cookie).execute().body());
            System.out.println(jsonObject);
            if (jsonObject.getInt("code") == 200 && jsonObject.getStr("msg").equals(Myconfig.getValue("user.id"))) {
                Toast.makeText(getApplicationContext(), "欢迎你新来的美食家", Toast.LENGTH_SHORT).show();
                return true;
            }
        }
        return false;
    }


}