package com.llw.changeavatar.Activity.Login;

import static com.llw.changeavatar.Activity.ButtomTab.activityList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.llw.changeavatar.Activity.shop.activity.entry.ShopCart;
import com.llw.changeavatar.Adapter.AssListAdapter;
import com.llw.changeavatar.Adapter.PurchaseListAdapter;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.AssListBean;
import com.llw.changeavatar.bean.PurchaseListBean;
import com.llw.changeavatar.utils.Const;
import com.llw.changeavatar.utils.Myconfig;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;

public class AssListActivity extends AppCompatActivity {
    private static Integer offset = 0;
    private static Integer page =   10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.assessment_alls);
        activityList.add(this);
        ShopCart data =(ShopCart)getIntent().getSerializableExtra("data");
        RecyclerView viewById = (RecyclerView) findViewById(R.id.home_recycler_assessment);
        Context mContext = this;
        ImageView back=findViewById(R.id.iv_back);
        back.setOnClickListener(view -> finish());
        List<AssListBean.DataBean> list = new ArrayList<>();
        new Thread(()->{
            HashMap<String, Object> from = new HashMap<>();
            if(Myconfig.getValue("user.id").length()==0){
                startActivity(new Intent(AssListActivity.this, MainActivity.class));
            }
            from.put("user_id",Myconfig.getValue("user.id"));
            from.put("offset",offset);
            from.put("page",page);
            String body = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/evaluation/queryByUserid").form(from).execute().body();
            runOnUiThread(()->{
                JSONArray data1 = new JSONObject(body).getJSONArray("data");

                for (Object item : data1) {
                    JSONObject jsonObject = new JSONObject(item);
                        list.add(new AssListBean.DataBean(jsonObject.getInt("star"),0,0,jsonObject.getStr("shoppic"),Myconfig.getValue("user.nickname"),jsonObject.getStr("context")+"#"+ (!StrUtil.isBlank(Myconfig.getValue("user.headpic") )?Myconfig.getValue("user.headpic"):"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png"),jsonObject.getStr("shoppic"),jsonObject.getStr("shopname"),jsonObject.getInt("id")));

                }
                AssListAdapter adapter = new AssListAdapter(mContext, list);
                viewById.setLayoutManager(new LinearLayoutManager(mContext));
                viewById.setAdapter(adapter);
            });
        }).start();
    }
}
