package com.llw.changeavatar.Activity.shop.activity.pop.clz.interfaces;

/**
 * Description:
 * Create by dance, at 2018/12/17
 */
public interface OnSelectListener {
    void onSelect(int position, String text);
}
