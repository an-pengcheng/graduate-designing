package com.llw.changeavatar.Activity.shop;

import static com.llw.changeavatar.Activity.ButtomTab.activityList;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.llw.changeavatar.Activity.Login.FavoritesActivity;
import com.llw.changeavatar.Activity.Login.MainActivity;
import com.llw.changeavatar.Activity.Login.SignUp;
import com.llw.changeavatar.Activity.TalkActivity;
import com.llw.changeavatar.Activity.shop.activity.BaseTrDarkActivity;

import com.llw.changeavatar.Activity.shop.activity.PayingActivity;
import com.llw.changeavatar.Activity.shop.activity.adapter.LeftProductTypeAdapter;
import com.llw.changeavatar.Activity.shop.activity.adapter.RightProductAdapter;
import com.llw.changeavatar.Activity.shop.activity.entry.EventBusShoppingEntity;
import com.llw.changeavatar.Activity.shop.activity.entry.ProductListEntity;
import com.llw.changeavatar.Activity.shop.activity.entry.ShopCart;
import com.llw.changeavatar.Activity.shop.activity.imp.ShopCartImp;
import com.llw.changeavatar.Activity.shop.activity.pop.CustomPartShadowPopupView;
import com.llw.changeavatar.Activity.shop.activity.pop.clz.XPopup;
import com.llw.changeavatar.Activity.shop.activity.view.JudgeNestedScrollView;
import com.llw.changeavatar.Adapter.AssListAdapter;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.AssListBean;
import com.llw.changeavatar.bean.DetailBean;
import com.llw.changeavatar.utils.Const;
import com.llw.changeavatar.utils.Myconfig;
import com.llw.changeavatar.utils.ScreenUtil;
import com.llw.changeavatar.utils.StatusBarUtil;
import com.llw.changeavatar.utils.Tool;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.listener.SimpleMultiPurposeListener;
import com.scwang.smartrefresh.layout.util.DensityUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;


import butterknife.BindView;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;

/**
 * @packageName:com.huidaxuan.ic2cloud.app2c.view.activity
 * @className: StoreDetailsQmActivityBf
 * @description:门店详情-本地生活的详情
 * @author: dingchao
 * @time: 2020-11-16 11:13
 */
public class StoreDetailsLifeActivity extends BaseTrDarkActivity implements View.OnClickListener, LeftProductTypeAdapter.onItemSelectedListener, ShopCartImp {
    @BindView(R.id.v_include_status_bar_height_dynamic)
    View v_include_status_bar_height_dynamic;

    @BindView(R.id.my_tv_app_title_text)
    TextView tv_app_title_text;
    @BindView(R.id.iv_app_title_left)
    ImageView iv_app_title_left;
    @BindView(R.id.iv_app_title_right)
    ImageView iv_app_title_right;
    @BindView(R.id.iv_app_title_right2)
    ImageView iv_app_title_right2;
    @BindView(R.id.rl_app_title_return)
    RelativeLayout rl_app_title_return;
    @BindView(R.id.rl_app_title_right)
    RelativeLayout rl_app_title_right;
    @BindView(R.id.my_iv_header)
    ImageView ivHeader;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rl_shopping_cart_all)
    RelativeLayout rl_shopping_cart_all;
    @BindView(R.id.scrollView)
    JudgeNestedScrollView scrollView;

    int toolBarPositionY = 0;
    private int mOffset = 0;
    private int mScrollY = 0;
    private int isfav;

    @BindView(R.id.tv_include_store_details_top_xxz)
    TextView tv_include_store_details_top_xxz;

    @BindView(R.id.cv_include_store_details_add_car)
    CardView cv_include_store_details_add_car;
    @BindView(R.id.tv_store_details_life_shopping_cart_title)
    TextView tv_store_details_life_shopping_cart_title;
    @BindView(R.id.tv_store_details_life_shopping_cart_title2)
    TextView tv_store_details_life_shopping_cart_title2;

    /*购物车区域*/
    @BindView(R.id.left_menu)//左侧列表
            RecyclerView leftMenu;
    @BindView(R.id.right_menu)//右侧列表
    RecyclerView rightMenu;
    @BindView(R.id.right_menu_item)//右侧标题整体
    LinearLayout headerLayout;
     @BindView(R.id.right_menu_tv)//右侧标题
    TextView headerView;

    private LeftProductTypeAdapter leftAdapter;
    private RightProductAdapter rightAdapter;

    ArrayList<ProductListEntity> productListEntities;
    ProductListEntity headMenu;

    private boolean leftClickType = false;//左侧菜单点击引发的右侧联动

    @BindView(R.id.rl)//动画效果二级列表 父容器
    RelativeLayout rl;
    @BindView(R.id.iv_shopping_cart_img_1)//动画效果底部购物车图标，最终落入的地方
    ImageView iv_shopping_cart_img;
    private PathMeasure mPathMeasure;
    /**
     * 贝塞尔曲线中间过程的点的坐标
     */
    private float[] mCurrentPosition = new float[2];
    @BindView(R.id.tv_shopping_cart_count_1)
    TextView tv_shopping_cart_count;

    //购物车无数据时要隐藏处理
    @BindView(R.id.tv_shopping_cart_money_1)
    TextView tv_shopping_cart_money;
    ShopCart shopCart;
    @BindView(R.id.btn_shopping_cart_pay_1)
    Button btn_shopping_cart_pay;
    @BindView(R.id.rl_bottom_shopping_cart_activity)
    RelativeLayout rl_bottom_shopping_cart;
    /*购物车区域*/
    @BindView(R.id.look_shop_assessment)
    Button look_shop_assessment;

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_app_title_return:
                finish();
                break;
            case R.id.btn_shopping_cart_pay_1://去结算
                if(shopCart.getShoppingTotalPrice() > 0){
                    Intent i = new Intent(StoreDetailsLifeActivity.this , PayingActivity.class);
                    i.putExtra("data",shopCart);
                    i.putExtra("shop_id",getIntent().getIntExtra("id", 1));
                    startActivity(i);
                    break;
                }
            case R.id.rl_bottom_shopping_cart_activity://打开购物车
                Log.e("getWindowHeight", "---------height:" + Tool.getWindowHeight(StoreDetailsLifeActivity.this));
                //获取屏幕的高度，然后拿到百分之70
                int popHeight = (int) (Tool.getWindowHeight(StoreDetailsLifeActivity.this) * 0.7);
                if (shopCart != null && shopCart.getShoppingAccount() > 0) {
                    new XPopup.Builder(StoreDetailsLifeActivity.this)
                            .atView(v)
                            .maxHeight(popHeight)
                            .isRequestFocus(false)
                            .asCustom(new CustomPartShadowPopupView(StoreDetailsLifeActivity.this, shopCart))
                            .show();
                }
                break;
            case R.id.iv_app_title_right2:
                Intent ii = new Intent(StoreDetailsLifeActivity.this , TalkActivity.class);
                Intent intent1 = getIntent();
                ii.putExtra("room_id",intent1.getIntExtra("id", 1));
                ii.putExtra("user_id",intent1.getIntExtra("user_id",1));
                startActivity(ii);
                break;
            case R.id.look_shop_assessment:
                Intent i1 = new Intent(StoreDetailsLifeActivity.this , ShopAssListActivity.class);
                Intent intent2 = getIntent();
                i1.putExtra("title",intent2.getStringExtra("title"));
                i1.putExtra("url",intent2.getStringExtra("url"));
                i1.putExtra("saleconunt",intent2.getStringExtra("salecount"));
                i1.putExtra("salemain",intent2.getStringExtra("salemain"));
                i1.putExtra("shop_id",intent2.getIntExtra("id", 1));
                i1.putExtra("user_id",intent2.getIntExtra("user_id",1));
                startActivity(i1);
                break;
            case R.id.rl_app_title_right:
                ImageView here = rl_app_title_right.findViewById(R.id.iv_app_title_right);
                if(isfav>0){
                    new Thread(()->{
                        HashMap<String, Object> from = new HashMap<>();
                        Intent intent = getIntent();
                        from.put("shopid",intent.getIntExtra("id", 1));
                        from.put("userid", Myconfig.getValue("user.id"));
                        String body = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/fav/remove").form(from).execute().body();
                        runOnUiThread(()->{
                            Integer data1 = new JSONObject(body).getInt("code");
                            if (data1==200){
                                Toast.makeText(StoreDetailsLifeActivity.this,"取消收藏",Toast.LENGTH_SHORT);
                            }
                        });
                    }).start();
                    here.setImageResource(R.mipmap.sc_white);
                    isfav=0;
                }else{
                    new Thread(()->{
                        HashMap<String, Object> from = new HashMap<>();
                        Intent intent = getIntent();
                        from.put("shopid",intent.getIntExtra("id", 1));
                        from.put("userid", Myconfig.getValue("user.id"));
                        String body = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/fav/add").form(from).execute().body();
                        runOnUiThread(()->{
                            Integer data1 = new JSONObject(body).getInt("code");
                            if (data1==200){
                                Toast.makeText(StoreDetailsLifeActivity.this,"收藏成功",Toast.LENGTH_SHORT);
                            }
                        });
                    }).start();
                    isfav=1;
                    here.setImageResource(R.mipmap.sc_black);
                }
                break;

        }
    }


    @Override
    protected int getLayout() {
        return R.layout.activity_store_details_life;
    }

    @Override
    protected void initEvent() {
        StatusBarUtil.setStatusBarDarkTheme(StoreDetailsLifeActivity.this, false);//白色
        Tool.setStatusBarHeight(StoreDetailsLifeActivity.this, v_include_status_bar_height_dynamic);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void noNetWork() {

    }

    @Override
    protected void initListener() {
        rl_app_title_return.setOnClickListener(this);
        btn_shopping_cart_pay.setOnClickListener(this);
        rl_bottom_shopping_cart.setOnClickListener(this);
        rl_app_title_right.setOnClickListener(this);
        iv_app_title_right2.setOnClickListener(this);
        look_shop_assessment.setOnClickListener(this);
        leftMenu.setLayoutManager(new LinearLayoutManager(this));
        rightMenu.setLayoutManager(new LinearLayoutManager(this));
        headerLayout.setVisibility(View.VISIBLE);

//        StickyHeaderLayoutManager stickyHeaderLayoutManager = new StickyHeaderLayoutManager();
//        rightMenu.setLayoutManager(stickyHeaderLayoutManager);
        //右侧列表监听
        rightMenu.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.canScrollVertically(1) == false) {//无法下滑
                    showHeadView();
                    return;
                }

                View underView = null;
                if (dy > 0) {
                    underView = rightMenu.findChildViewUnder(headerLayout.getX(), headerLayout.getMeasuredHeight() + 1);
                } else {
                    underView = rightMenu.findChildViewUnder(headerLayout.getX(), 0);
                }

                if (underView != null && underView.getContentDescription() != null) {
                    int position = Integer.parseInt(underView.getContentDescription().toString());
                    ProductListEntity menu = rightAdapter.getMenuOfMenuByPosition(position);

                    if (leftClickType || !menu.getTypeName().equals(headMenu.getTypeName())) {
                        if (dy > 0 && headerLayout.getTranslationY() <= 1 && headerLayout.getTranslationY() >= -1 * headerLayout.getMeasuredHeight() * 4 / 5 && !leftClickType) {// underView.getTop()>9
                            int dealtY = underView.getTop() - headerLayout.getMeasuredHeight();
                            headerLayout.setTranslationY(dealtY);
                        } else if (dy < 0 && headerLayout.getTranslationY() <= 0 && !leftClickType) {
                            headerView.setText(menu.getTypeName());
                            int dealtY = underView.getBottom() - headerLayout.getMeasuredHeight();
                            headerLayout.setTranslationY(dealtY);
                        } else {
                            headerLayout.setTranslationY(0);
                            headMenu = menu;
                            headerView.setText(headMenu.getTypeName());
                            for (int i = 0; i < productListEntities.size(); i++) {
                                if (productListEntities.get(i) == headMenu) {
                                    leftAdapter.setSelectedNum(i);
                                    break;
                                }
                            }
                            if (leftClickType) leftClickType = false;
                        }
                    }
                }

            }
        });
        initView();
    }

    private void initView() {
        tv_include_store_details_top_xxz.setVisibility(View.VISIBLE);
        cv_include_store_details_add_car.setVisibility(View.VISIBLE);
        if(Objects.equals(getIntent().getStringExtra("deliver"), "1")){
            ((TextView)findViewById(R.id.tv_include_store_details_top_xxz)).setText("店铺状态：空闲");
            ((TextView)findViewById(R.id.tv_include_store_details_top_xxz)).setTextColor(Color.rgb(0,255,0));
            ((TextView)findViewById(R.id.tv_include_store_details_top_xxz)).setBackgroundColor(Color.rgb(202 ,255 ,112));
            ((TextView)findViewById(R.id.tv_item_service_owner_type)).setText("空闲");
            ((TextView)findViewById(R.id.tv_item_service_owner_type)).setTextColor(Color.rgb(0,255,0));

        }
        if(Objects.equals(getIntent().getStringExtra("deliver"), "2")){
            ((TextView)findViewById(R.id.tv_include_store_details_top_xxz)).setText("店铺状态：忙碌");
            ((TextView)findViewById(R.id.tv_include_store_details_top_xxz)).setTextColor(Color.rgb(255 ,215, 0));
            ((TextView)findViewById(R.id.tv_include_store_details_top_xxz)).setBackgroundColor(Color.rgb(255, 255 ,224));
            ((TextView)findViewById(R.id.tv_item_service_owner_type)).setText("忙碌");
            ((TextView)findViewById(R.id.tv_item_service_owner_type)).setTextColor(Color.rgb(255 ,215, 0));
        }
        if(Objects.equals(getIntent().getStringExtra("deliver"), "3")){
            ((TextView)findViewById(R.id.tv_include_store_details_top_xxz)).setText("店铺状态：拥挤");
            ((TextView)findViewById(R.id.tv_include_store_details_top_xxz)).setTextColor(Color.rgb(238 ,99 ,99));
            ((TextView)findViewById(R.id.tv_include_store_details_top_xxz)).setBackgroundColor(Color.rgb(255,160,122));
            ((TextView)findViewById(R.id.tv_item_service_owner_type)).setText("拥挤");
            ((TextView)findViewById(R.id.tv_item_service_owner_type)).setTextColor(Color.rgb(238 ,99 ,99));

        }
        if(Objects.equals(getIntent().getStringExtra("deliver"), "4")){
            ((TextView)findViewById(R.id.tv_include_store_details_top_xxz)).setText("店铺状态：爆满");
            ((TextView)findViewById(R.id.tv_include_store_details_top_xxz)).setTextColor(Color.rgb(145, 44 ,238));
            ((TextView)findViewById(R.id.tv_include_store_details_top_xxz)).setBackgroundColor(Color.rgb(238 ,130 ,238));
            ((TextView)findViewById(R.id.tv_item_service_owner_type)).setText("爆满");
            ((TextView)findViewById(R.id.tv_item_service_owner_type)).setTextColor(Color.rgb(145, 44 ,238));

        }
        //下拉监听，禁用
        refreshLayout.setOnMultiPurposeListener(new SimpleMultiPurposeListener() {
            @Override
            public void onHeaderPulling(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
                ivHeader.setTranslationY(mOffset - mScrollY);
                toolbar.setAlpha(1 - Math.min(percent, 1));
            }

            @Override
            public void onHeaderReleasing(RefreshHeader header, float percent, int offset, int bottomHeight, int extendHeight) {
                mOffset = offset / 2;
                ivHeader.setTranslationY(mOffset - mScrollY);
                toolbar.setAlpha(1 - Math.min(percent, 1));
            }
        });
        refreshLayout.setEnableRefresh(false);

        toolbar.post(new Runnable() {
            @Override
            public void run() {
                dealWithViewPager();
            }
        });

        //scrollView滑动监听
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            int lastScrollY = 0;
            int h = DensityUtil.dp2px(211);
            int color = ContextCompat.getColor(getApplicationContext(), R.color.color_ffffff) & 0x00ffffff;

            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                int[] location = new int[2];
                tv_store_details_life_shopping_cart_title.getLocationOnScreen(location);
                int yPosition = location[1];
                if (yPosition < toolBarPositionY) {
                    tv_store_details_life_shopping_cart_title2.setVisibility(View.VISIBLE);
                    tv_store_details_life_shopping_cart_title2.setBackgroundResource(R.color.color_ffffff);
                    scrollView.setNeedScroll(false);
                } else {
                    tv_store_details_life_shopping_cart_title2.setVisibility(View.GONE);
                    scrollView.setNeedScroll(true);

                }

                if (lastScrollY < h) {
                    scrollY = Math.min(h, scrollY);
                    mScrollY = scrollY > h ? h : scrollY;
                    tv_app_title_text.setAlpha(1f * mScrollY / h);
                    toolbar.setBackgroundColor(((255 * mScrollY / h) << 24) | color);
                    ivHeader.setTranslationY(mOffset - mScrollY);
                }
                if (scrollY == 0) {
                    iv_app_title_left.setImageResource(R.mipmap.back_white);
                    if(isfav==0){
                        iv_app_title_right.setImageResource(R.mipmap.sc_white);
                    }else {
                        iv_app_title_right.setImageResource(R.mipmap.sc_black);
                    }

//                    ivMenu.setImageResource(R.drawable.icon_menu_white);
                } else {
                    iv_app_title_left.setImageResource(R.mipmap.iv_app_return);
                    if(isfav==0){
                        iv_app_title_right.setImageResource(R.mipmap.sc_white);
                    }else {
                        iv_app_title_right.setImageResource(R.mipmap.sc_black);
                    }
//                    ivMenu.setImageResource(R.drawable.icon_menu_black);
                }

                lastScrollY = scrollY;
            }
        });
        tv_app_title_text.setAlpha(0);
        toolbar.setBackgroundColor(0);


    }

    /**
     * 高度设定
     */
    private void dealWithViewPager() {
        toolBarPositionY = toolbar.getHeight();
        ViewGroup.LayoutParams params = rl_shopping_cart_all.getLayoutParams();
        params.height = ScreenUtil.getScreenHeightPx(getApplicationContext())
                - toolBarPositionY
                - tv_store_details_life_shopping_cart_title.getHeight()
//                - tv_store_details_life_shopping_cart_title.getHeight()
                - tv_store_details_life_shopping_cart_title.getHeight() + 1;
        rl_shopping_cart_all.setLayoutParams(params);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);//eventbus解绑
        leftAdapter.removeItemSelectedListener(this);
    }

    @Override
    protected void initData() {
        //列表数据初始化
        initListData();
    }

    /**
     * 虚拟列表数据
     */
    private void initListData() {
        productListEntities = new ArrayList<>();
        HashMap<String, ArrayList<ProductListEntity.ProductEntity>> itemlists = new HashMap<>();
        shopCart = new ShopCart();
        Intent intent = getIntent();
        int id = intent.getIntExtra("id", 0);
         isfav = intent.getIntExtra("isfav",0);
        ImageView here = rl_app_title_right.findViewById(R.id.iv_app_title_right);
        new Thread(()-> {
            String body = HttpUtil.createGet(Const.PUBLIC_SERVER + "/api/fav/isfav?userid=" + Myconfig.getValue("user.id") + "&shopid=" + id).execute().body().trim();
            Integer jsonObject = new JSONObject(body).getInt("data");
            isfav=jsonObject;
            if(isfav!=0){
                here.setImageResource(R.mipmap.sc_black);
            }else{
                here.setImageResource(R.mipmap.sc_white);
            }

        }).start();

        StoreDetailsLifeActivity storeDetailsLifeActivity = this;
        new Thread(new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                List<ProductListEntity.ProductEntity> list = new ArrayList<>();
                String body = HttpUtil.createGet(Const.PUBLIC_SERVER + "/api/shopitem/query?id=" + id).execute().body().trim();
                JSONArray data = new JSONObject(body).getJSONArray("data");
                int len = data.size();
                for (int i = 0; i < len; i++) {
                    JSONObject item = data.getJSONObject(i);
                    if (!itemlists.containsKey(item.getStr("cate"))) {
                        itemlists.put(item.getStr("cate"), new ArrayList<>());
                    }
                    itemlists.get(item.getStr("cate")).add(new ProductListEntity.ProductEntity(item.getStr("item_url"), item.getStr("name"), item.getStr("moonsale"), Double.valueOf(item.getStr("nowprice")), 0, item.getInt("id")+"", item.getStr("cate")));
                }
                Set<Map.Entry<String, ArrayList<ProductListEntity.ProductEntity>>> entries = itemlists.entrySet();
                int index = 0;
                for (Map.Entry<String, ArrayList<ProductListEntity.ProductEntity>> entry : entries) {
                    productListEntities.add(new ProductListEntity(++index + "", entry.getKey(), entry.getValue()));
                }
                //设置数据源，数据绑定展示
                leftAdapter = new LeftProductTypeAdapter(StoreDetailsLifeActivity.this, productListEntities);
                rightAdapter = new RightProductAdapter(StoreDetailsLifeActivity.this, productListEntities, shopCart);
                runOnUiThread(() -> {
                    rightMenu.setAdapter(rightAdapter);
                    leftMenu.setAdapter(leftAdapter);
                    //左侧列表单项选择
                    leftAdapter.addItemSelectedListener(storeDetailsLifeActivity);
                    rightAdapter.setShopCartImp(storeDetailsLifeActivity);
                    //设置初始头部
                    initHeadView();
                });
            }
        }).start();


    }


    /**
     * 初始头部
     */
    private void initHeadView() {
        headMenu = rightAdapter.getMenuOfMenuByPosition(0);
        headerLayout.setContentDescription("0");
        if(headMenu!=null)
        headerView.setText(headMenu.getTypeName());
    }

    /**
     * 显示标题
     */
    private void showHeadView() {
        headerLayout.setTranslationY(0);
        View underView = rightMenu.findChildViewUnder(headerLayout.getX(), 0);
        if (underView != null && underView.getContentDescription() != null) {
            int position = Integer.parseInt(underView.getContentDescription().toString());
            ProductListEntity entity = rightAdapter.getMenuOfMenuByPosition(position + 1);
            headMenu = entity;
            headerView.setText(headMenu.getTypeName());
            for (int i = 0; i < productListEntities.size(); i++) {
                if (productListEntities.get(i) == headMenu) {
                    leftAdapter.setSelectedNum(i);
                    break;
                }
            }
        }
    }

    @Override
    protected void savedInstanceState(Bundle savedInstanceState) {

    }

    /**
     * 左侧列表单项选中
     *
     * @param position
     * @param menu
     */
    @Override
    public void onLeftItemSelected(int position, ProductListEntity menu) {
        int sum = 0;
        for (int i = 0; i < position; i++) {
            sum += productListEntities.get(i).getProductEntities().size() + 1;
        }
        LinearLayoutManager layoutManager = (LinearLayoutManager) rightMenu.getLayoutManager();
        rightMenu.scrollToPosition(position);
        layoutManager.scrollToPositionWithOffset(sum, 0);
        leftClickType = true;
    }

    /**
     * 购物车+
     *
     * @param view
     * @param position
     */
    @Override
    public void add(View view, int position, ProductListEntity.ProductEntity entity) {
        addCart(view, entity);
    }

    /**
     * 加入购物车曲线动画
     *
     * @param view
     * @param entity
     */
    private void addCart(View view, ProductListEntity.ProductEntity entity) {
//   一、创造出执行动画的主题---imageview
        //代码new一个imageview，图片资源是上面的imageview的图片
        // (这个图片就是执行动画的图片，从开始位置出发，经过一个抛物线（贝塞尔曲线），移动到购物车里)
        final ImageView goods = new ImageView(StoreDetailsLifeActivity.this);
        goods.setImageDrawable(getResources().getDrawable(R.drawable.shape_shopping_cart_num_bg, null));
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
        rl.addView(goods, params);

//    二、计算动画开始/结束点的坐标的准备工作
        //得到父布局的起始点坐标（用于辅助计算动画开始/结束时的点的坐标）
        int[] parentLocation = new int[2];
        rl.getLocationInWindow(parentLocation);

        //得到商品图片的坐标（用于计算动画开始的坐标）
        int startLoc[] = new int[2];
        view.getLocationInWindow(startLoc);

        //得到购物车图片的坐标(用于计算动画结束后的坐标)
        int endLoc[] = new int[2];
        iv_shopping_cart_img.getLocationInWindow(endLoc);


//    三、正式开始计算动画开始/结束的坐标
        //开始掉落的商品的起始点：商品起始点-父布局起始点+该商品图片的一半
        float startX = startLoc[0] - parentLocation[0] + goods.getWidth() / 2;
        float startY = startLoc[1] - parentLocation[1] + goods.getHeight() / 2;

        //商品掉落后的终点坐标：购物车起始点-父布局起始点+购物车图片的1/5
        float toX = endLoc[0] - parentLocation[0] + iv_shopping_cart_img.getWidth() / 5;
        float toY = endLoc[1] - parentLocation[1];

//    四、计算中间动画的插值坐标（贝塞尔曲线）（其实就是用贝塞尔曲线来完成起终点的过程）
        //开始绘制贝塞尔曲线
        Path path = new Path();
        //移动到起始点（贝塞尔曲线的起点）
        path.moveTo(startX, startY);
        //使用二次萨贝尔曲线：注意第一个起始坐标越大，贝塞尔曲线的横向距离就会越大，一般按照下面的式子取即可
        path.quadTo((startX + toX) / 2, startY, toX, toY);
        //mPathMeasure用来计算贝塞尔曲线的曲线长度和贝塞尔曲线中间插值的坐标，
        // 如果是true，path会形成一个闭环
        mPathMeasure = new PathMeasure(path, false);

        //★★★属性动画实现（从0到贝塞尔曲线的长度之间进行插值计算，获取中间过程的距离值）
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, mPathMeasure.getLength());
        valueAnimator.setDuration(500);
        // 匀速线性插值器
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // 当插值计算进行时，获取中间的每个值，
                // 这里这个值是中间过程中的曲线长度（下面根据这个值来得出中间点的坐标值）
                float value = (Float) animation.getAnimatedValue();
                // ★★★★★获取当前点坐标封装到mCurrentPosition
                // boolean getPosTan(float distance, float[] pos, float[] tan) ：
                // 传入一个距离distance(0<=distance<=getLength())，然后会计算当前距
                // 离的坐标点和切线，pos会自动填充上坐标，这个方法很重要。
                mPathMeasure.getPosTan(value, mCurrentPosition, null);//mCurrentPosition此时就是中间距离点的坐标值
                // 移动的商品图片（动画图片）的坐标设置为该中间点的坐标
                goods.setTranslationX(mCurrentPosition[0]);
                goods.setTranslationY(mCurrentPosition[1]);
            }
        });
//   五、 开始执行动画
        valueAnimator.start();

//   六、动画结束后的处理
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            //当动画结束后：
            @Override
            public void onAnimationEnd(Animator animation) {
                //更新底部数据
                showTotalPrice(entity);
                // 把移动的图片imageview从父布局里移除
                rl.removeView(goods);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    /**
     * 底部价格和数量显示
     */
    private void showTotalPrice(ProductListEntity.ProductEntity entity) {
        if (shopCart != null && shopCart.getShoppingTotalPrice() > 0) {
            btn_shopping_cart_pay.setBackgroundResource(R.drawable.shape_btn_shopping_cart_bg1);
            tv_shopping_cart_money.setVisibility(View.VISIBLE);
            tv_shopping_cart_money.setText("￥ " + shopCart.getShoppingTotalPrice());
            tv_shopping_cart_count.setVisibility(View.VISIBLE);
            //得到总的数量
            int textCount = 0;
            for (ProductListEntity.ProductEntity m : shopCart.getShoppingSingle().keySet()) {
                Log.e("btn_shopping_cart_pay", "map集合中存储的数据---->" + m.getProductCount());
                textCount += m.getProductCount();
            }

            tv_shopping_cart_count.setText("" + textCount);
        } else {
            btn_shopping_cart_pay.setBackgroundResource(R.drawable.shape_btn_shopping_cart_bg);
            tv_shopping_cart_money.setVisibility(View.INVISIBLE);
            tv_shopping_cart_count.setVisibility(View.GONE);
        }
        updateLeftCount(entity);
    }

    /**
     * 更新左侧数字角标(暂时不包含清空)，触发更新肯定是在加或者减的时候触发,根据子项中的父ID和左侧ID比对，
     */
    private void updateLeftCount(ProductListEntity.ProductEntity entity) {
        if (shopCart != null) {
            //加和减的时候要知道是那个左侧下边的,知道下标获取父id,然后从map中取count
            if (entity != null) {
                Log.e("updateLeftCount", "-------parentId:" + entity.getParentId() + "---------count:" + shopCart.getParentCountMap().get(entity.getParentId()));
                leftAdapter.setUpdateMenuCount(entity.getParentId(), shopCart.getParentCountMap().get(entity.getParentId()));
            }
            if (rightAdapter != null) rightAdapter.notifyDataSetChanged();//跟新列表
        }
    }

    /**
     * 购物车减
     *
     * @param view
     * @param position
     */
    @Override
    public void remove(View view, int position, ProductListEntity.ProductEntity en) {
        showTotalPrice(en);
    }

    /**
     * 清空购物车及左侧列表都角标和商品列表
     */
    private void clearCartDataAndListData() {
        shopCart.clear();
        shopCart.getParentCountMap().clear();
        showTotalPrice(null);
        //左侧清空
        leftAdapter.setClearCount();
    }

    //定义处理接收的方法
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventBusShoppingEntity entity) {
        if (entity.getKey().equals("add")) {
            showTotalPrice(entity.getEntity());
        } else if (entity.getKey().equals("reduce")) {
            showTotalPrice(entity.getEntity());
        } else if (entity.getKey().equals("clearAll")) {//清空全部
            clearCartDataAndListData();
        }
    }

}
