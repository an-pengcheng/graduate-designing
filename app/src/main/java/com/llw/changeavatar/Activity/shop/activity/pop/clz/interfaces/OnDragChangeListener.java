package com.llw.changeavatar.Activity.shop.activity.pop.clz.interfaces;

public interface OnDragChangeListener {
    void onRelease();
    void onDragChange(int dy, float scale, float fraction);
}
