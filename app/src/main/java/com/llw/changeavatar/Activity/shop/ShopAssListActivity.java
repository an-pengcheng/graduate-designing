package com.llw.changeavatar.Activity.shop;

import static com.llw.changeavatar.Activity.ButtomTab.activityList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.util.Base64;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.llw.changeavatar.Activity.shop.activity.entry.ShopCart;
import com.llw.changeavatar.Adapter.AssListAdapter;
import com.llw.changeavatar.Adapter.ShopAssListAdapter;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.AssListBean;
import com.llw.changeavatar.bean.DetailBean;
import com.llw.changeavatar.bean.HomeListBean;
import com.llw.changeavatar.bean.ShopAssListBean;
import com.llw.changeavatar.utils.Const;
import com.llw.changeavatar.utils.Myconfig;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
public class ShopAssListActivity extends AppCompatActivity {
    private static Integer offset = 0;
    private static Integer page =   10;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent=getIntent();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_main);
        activityList.add(this);
        TextView detail_title=findViewById(R.id.detail_title);
        detail_title.setText(intent.getStringExtra("title"));
        TextView detail_sale=findViewById(R.id.detail_deliver);
        detail_sale.setText("已售出："+intent.getStringExtra("saleconunt"));
        TextView detail_main=findViewById(R.id.detail_announce);
        detail_main.setText("主营："+intent.getStringExtra("salemain"));
        ImageView back=findViewById(R.id.iv_back);
        ImageView shopPic=findViewById(R.id.detail_image);
        Picasso.get().load(intent.getStringExtra("url")).into(shopPic);
        back.setOnClickListener(view -> finish());
        RecyclerView viewById = (RecyclerView) findViewById(R.id.shop_assessment);
        Context mContext = this;
        List<ShopAssListBean.DataBean> list = new ArrayList<>();
        new Thread(()->{
            HashMap<String, Object> from = new HashMap<>();
            from.put("shopid", getIntent().getIntExtra("shop_id",1));
            from.put("offset",offset);
            from.put("page",page);
            String body = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/evaluation/query").form(from).execute().body();

                JSONArray data1 = new JSONObject(body).getJSONArray("data");
                for (Object item : data1) {
                    JSONObject jsonObject = new JSONObject(item);
                    HashMap<String, Object> from1 = new HashMap<>();
                    from1.put("id",jsonObject.getStr("user_id"));
                    String body1 = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/user/getinfo").form(from1).execute().body();
                    JSONObject jsonObject1 = new JSONObject(body1).getJSONObject("data");
                    list.add(new ShopAssListBean.DataBean(getIntent().getIntExtra("user_id",1),getIntent().getIntExtra("shop_id",1),jsonObject1.getStr("nickname"),jsonObject1.getStr("headpic"),jsonObject.getStr("star"),jsonObject.getStr("context")));
                }
            runOnUiThread(()->{
                ShopAssListAdapter adapter = new ShopAssListAdapter(mContext, list);
                viewById.setLayoutManager(new LinearLayoutManager(mContext));
                viewById.setAdapter(adapter);
            });
        }).start();
    }
}