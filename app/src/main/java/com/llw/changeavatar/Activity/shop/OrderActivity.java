package com.llw.changeavatar.Activity.shop;

import static com.llw.changeavatar.Activity.ButtomTab.activityList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.TimePickerView;
import com.llw.changeavatar.Activity.Login.SignUp;
import com.llw.changeavatar.Activity.Login.UserInfo;
import com.llw.changeavatar.Adapter.BuyListAdapter;
import com.llw.changeavatar.Adapter.PurchaseListAdapter;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.BuyListBean;
import com.llw.changeavatar.bean.DetailBean;
import com.llw.changeavatar.bean.PurchaseListBean;
import com.llw.changeavatar.utils.Const;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;

public class OrderActivity extends AppCompatActivity {
    TimePickerView pvTime;
    Context mContext=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        activityList.add(this);
        Intent intent = getIntent();
        TextView order_list_comment= findViewById(R.id.order_list_comment);
        TextView detail_list_toolbar= findViewById(R.id.detail_list_toolbar);
        TextView order_list_price =findViewById(R.id.order_list_price);
        order_list_price.setText(intent.getStringExtra("order_price"));
        if(intent.getIntExtra("ispay",0)!=0){
            detail_list_toolbar.setText("未支付");
            order_list_comment.setText("立即付款");
        }else{
            detail_list_toolbar.setText("订单已完成");
            order_list_comment.setText("立即评价");
        }
        order_list_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id= intent.getIntExtra("id",1);
                new Thread(()->{
                   String a= HttpUtil.createGet(Const.PUBLIC_SERVER + "/api/order/pay?orderid="+id).execute().body().trim();
                    System.out.println(a+"    99999999999999999999999999999999999999999999999999999");
                    finish();
                }).start();
            }
        });
        RecyclerView viewById = (RecyclerView) findViewById(R.id.detail_recycler1);
        Context mContext = this;
        List<PurchaseListBean.DataBean> list = new ArrayList<>();
        String items=intent.getStringExtra("order_items");
        JSONArray itemlist= new JSONArray(items);
        int len =itemlist.size();
        for (int i = 0; i < len; i++) {
            JSONObject item = itemlist.getJSONObject(i);
            list.add(new PurchaseListBean.DataBean(item.getStr("item_url"),item.getStr("name"),item.getStr("context"),item.getInt("count"),item.getInt("nowprice")));
        }
        PurchaseListAdapter adapter = new PurchaseListAdapter(mContext, list);
        viewById.setLayoutManager(new LinearLayoutManager(mContext));
        viewById.setAdapter(adapter);

        TextView day_select = findViewById(R.id.day_select);
        day_select .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //点击组件的点击事件
                if (pvTime!=null){
                    pvTime.show(day_select);
                }
            }
        });
        Calendar selectedDate = Calendar.getInstance();
        Calendar startDate = Calendar.getInstance();
        int year = startDate.get(Calendar.YEAR);
        int month = startDate.get(Calendar.MONTH);
        int day = startDate.get(Calendar.DAY_OF_MONTH);
        int hour = startDate.get(Calendar.HOUR_OF_DAY);
        int minute = startDate.get(Calendar.MINUTE);
        startDate.setTime(new Date());
        Calendar endDate = Calendar.getInstance();
        endDate.set(year, 12, 31,23,59);
        //时间选择器
        pvTime = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                // 这里回调过来的v,就是show()方法里面所添加的 View 参数，如果show的时候没有添加参数，v则为null
                TextView btn = (TextView) v;
                Date date1 = new Date();
                if( date1.compareTo(date)<1){
                    btn.setText(getTimes(date));
                }else {
                    Toast.makeText(mContext,"日期不对",Toast.LENGTH_SHORT).show();
                    btn.setText("");
                }
            }
        })
                .setTitleText("预定时间")
                .setType(new boolean[]{false, true, true, true, true, false})
                .setLabel("年", "月", "日", "时", "分", "秒")
                .isCenterLabel(true)
                .setDividerColor(Color.DKGRAY)
                .setContentSize(16)//字号
                .setDate(selectedDate)
                .setRangDate(startDate, endDate)
                .setDecorView(null)
                .build();
    }
    private String getTimes(Date date) {//年月日时分秒格式
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return format.format(date);
    }
}