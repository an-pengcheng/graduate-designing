package com.llw.changeavatar.Activity.shop.activity.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.llw.changeavatar.Activity.Login.MainActivity;
import com.llw.changeavatar.Activity.Login.SignUp;
import com.llw.changeavatar.Activity.shop.MeituanActivity;
import com.llw.changeavatar.Activity.shop.StoreDetailsLifeActivity;
import com.llw.changeavatar.Activity.shop.activity.entry.ProductListEntity;
import com.llw.changeavatar.Activity.shop.activity.entry.ShopCart;
import com.llw.changeavatar.Activity.shop.activity.imp.ShopCartImp;
import com.llw.changeavatar.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * 备份
 */
public class RightProductAdapter extends RecyclerView.Adapter {
    private final int MENU_TYPE = 0;
    private final int DISH_TYPE = 1;
    private final int HEAD_TYPE = 2;

    private Context mContext;
    private ArrayList<ProductListEntity> data;

    private int mItemCount;


    private ShopCart shopCart;
    private ShopCartImp shopCartImp;


    public RightProductAdapter(Context mContext, ArrayList<ProductListEntity> mMenuList, ShopCart shopCart) {
        this.mContext = mContext;
        this.data = mMenuList;
        this.mItemCount = mMenuList.size();
        this.shopCart = shopCart;
        for (ProductListEntity menu : mMenuList) {
            mItemCount += menu.getProductEntities().size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        int sum = 0;
        for (ProductListEntity menu : data) {
            if (position == sum) {
                return MENU_TYPE;
            }
            sum += menu.getProductEntities().size() + 1;
        }
        return DISH_TYPE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == MENU_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.include_right_item_title, parent, false);
            MenuViewHolder viewHolder = new MenuViewHolder(view);
            return viewHolder;
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_life_product_bf, parent, false);
            DishViewHolder viewHolder = new DishViewHolder(view);
            return viewHolder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (getItemViewType(position) == MENU_TYPE) {
            MenuViewHolder menuholder = (MenuViewHolder) holder;
            if (menuholder != null) {
                menuholder.right_menu_title.setText(getMenuByPosition(position).getTypeName());
                menuholder.right_menu_layout.setContentDescription(position + "");
            }
        } else {
            final DishViewHolder dishholder = (DishViewHolder) holder;
            if (dishholder != null) {
                //根据下标取商品,当前这个position是右侧（所有展示数据）的下标
                final ProductListEntity.ProductEntity dish = getDishByPosition(position);
                //根据当前的下标获取父ID
                final int posss = getDishPositionByOnePosition(position);
                dishholder.tv_item_life_product_name.setText(dish.getProductName());
                dishholder.tv_item_life_product_monty.setText("月售 " + dish.getProductMonth());
                dishholder.tv_item_life_product_money.setText("" + dish.getProductMoney()/100);
                dishholder.right_dish_layout.setContentDescription(position + "");

                dishholder.tv_group_list_item_count_num.setText(dish.getProductCount() + "");
                if(dish.getProductImg()!=null&&!dish.getProductImg().equals("")){
                    Picasso.get().load(dish.getProductImg()).into(dishholder.iv_item_life_product);
                }
                int count = 0;
                if (shopCart.getShoppingSingle().containsKey(dish)) {
                    count = shopCart.getShoppingSingle().get(dish);
                }

                if (count <= 0) {
                    dishholder.tv_group_list_item_count_num.setVisibility(View.INVISIBLE);
                    dishholder.iv_group_list_item_count_reduce.setVisibility(View.INVISIBLE);
                } else {
                    dishholder.tv_group_list_item_count_num.setVisibility(View.VISIBLE);
                    dishholder.iv_group_list_item_count_reduce.setVisibility(View.VISIBLE);
                    dishholder.tv_group_list_item_count_num.setText(count + "");
                }
                dishholder.iv_item_life_product.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(mContext, MeituanActivity.class);
                        intent.putExtra("food_pic",dish.getProductImg());
                        intent.putExtra("food_name",dish.getProductName());
                        intent.putExtra("food_moonsale",dish.getProductMonth());
                        intent.putExtra("food_price",""+dish.getProductMoney()/100);
                        mContext.startActivity(intent);
                    }
                });
                //加减点击时间
                dishholder.iv_group_list_item_count_add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.e("posss", "-------------------posss:" + posss);
                        if (shopCart.addShoppingSingle(dish)) {
//                            notifyItemChanged(position);
                            //当前数字变化刷新
                            notifyDataSetChanged();
                            if (shopCartImp != null) {
                                shopCartImp.add(view, position,dish);

                            }
                        }
                    }
                });

                dishholder.iv_group_list_item_count_reduce.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (shopCart.subShoppingSingle(dish)) {
//                            notifyItemChanged(position);
                            //当前数字变化刷新
                            notifyDataSetChanged();
                            if (shopCartImp != null)
                                shopCartImp.remove(view, position,dish);

                        }
                    }
                });
            }
        }
    }

    public ShopCartImp getShopCartImp() {
        return shopCartImp;
    }

    public void setShopCartImp(ShopCartImp shopCartImp) {
        this.shopCartImp = shopCartImp;
    }

    public ProductListEntity getMenuByPosition(int position) {
        int sum = 0;
        for (ProductListEntity menu : data) {
            if (position == sum) {
                return menu;
            }
            sum += menu.getProductEntities().size() + 1;
        }
        return null;
    }

    public ProductListEntity.ProductEntity getDishByPosition(int position) {
        for (ProductListEntity menu : data) {
            if (position > 0 && position <= menu.getProductEntities().size()) {
                return menu.getProductEntities().get(position - 1);
            } else {
                position -= menu.getProductEntities().size() + 1;
            }
        }
        return null;
    }

    /**
     * 根据夫下标获取当前具体商品的下标
     *
     * @param position 父下标
     * @return
     */
    public int getDishPositionByOnePosition(int position) {
        for (ProductListEntity menu : data) {
            if (position > 0 && position <= menu.getProductEntities().size()) {
                return position - 1;
            } else {
                position -= menu.getProductEntities().size() + 1;
            }
        }
        return 0;
    }

    public ProductListEntity getMenuOfMenuByPosition(int position) {
        for (ProductListEntity menu : data) {
            if (position == 0) return menu;
            if (position > 0 && position <= menu.getProductEntities().size()) {
                return menu;
            } else {
                position -= menu.getProductEntities().size() + 1;
            }
        }
        return null;
    }


    @Override
    public int getItemCount() {
        return mItemCount;
    }


    private class MenuViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout right_menu_layout;
        private TextView right_menu_title;

        public MenuViewHolder(View itemView) {
            super(itemView);
            right_menu_layout = (LinearLayout) itemView.findViewById(R.id.right_menu_item);
            right_menu_title = (TextView) itemView.findViewById(R.id.right_menu_tv);
        }
    }

    private class DishViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_item_life_product_name;
        private TextView tv_item_life_product_monty;
        private TextView tv_item_life_product_money;
        private LinearLayout right_dish_layout;
        private ImageView iv_item_life_product;
        private ImageView iv_group_list_item_count_reduce;
        private TextView tv_group_list_item_count_num;
        private ImageView iv_group_list_item_count_add;

        public DishViewHolder(View itemView) {
            super(itemView);
            tv_item_life_product_name = (TextView) itemView.findViewById(R.id.tv_item_life_product_name);
            tv_item_life_product_monty = (TextView) itemView.findViewById(R.id.tv_item_life_product_monty);
            tv_item_life_product_money = (TextView) itemView.findViewById(R.id.tv_item_life_product_money);
            iv_item_life_product = (ImageView)itemView.findViewById(R.id.iv_item_life_product);
            right_dish_layout = (LinearLayout) itemView.findViewById(R.id.right_dish_item);

            iv_group_list_item_count_reduce = (ImageView) itemView.findViewById(R.id.iv_group_list_item_count_reduce);
            tv_group_list_item_count_num = (TextView) itemView.findViewById(R.id.tv_group_list_item_count_num);
            iv_group_list_item_count_add = (ImageView) itemView.findViewById(R.id.iv_group_list_item_count_add);
        }

    }
}
