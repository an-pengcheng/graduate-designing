package com.llw.changeavatar.Activity.Login;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.llw.changeavatar.Activity.ButtomTab;
import com.llw.changeavatar.Activity.shop.AssessmentActivity;
import com.llw.changeavatar.R;
import com.llw.changeavatar.utils.Const;
import com.llw.changeavatar.utils.Myconfig;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cn.hutool.core.map.MapUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;

public class QualityActivity extends Activity {

    private static final String[] m={"A型","B型","O型","AB型","其他"};
    private TextView view ;
    private List<String> realdata;
    private Spinner spinner;
    private List<Map<String,Object>> data;
    private ArrayAdapter<String> adapter;
    private Integer nowindex=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quality);
        data = new LinkedList<>();
        realdata = new LinkedList<>();
        EditText textAll=findViewById(R.id.assessment);
        ImageView back=findViewById(R.id.iv_back);
        back.setOnClickListener(view -> finish());
        view = (TextView) findViewById(R.id.spinnerText);
        spinner = (Spinner) findViewById(R.id.Spinner01);
        //将可选内容与ArrayAdapter连接起来
        //设置默认值
        new Thread(()->{
            String body = HttpUtil.createGet(Const.PUBLIC_SERVER + "/api/quality/getAllshopIdList").execute().body();
            JSONObject jsonObject = new JSONObject(body);
            JSONArray datas = jsonObject.getJSONArray("data");
            int size = datas.size();
            for(int i=0;i<size;i++){
                JSONObject jsonObject1 = datas.getJSONObject(i);
                HashMap<String, Object> d1= new HashMap<>();
                d1.put("name",jsonObject1.getStr("shopName"));
                d1.put("id",jsonObject1.getInt("id"));
                realdata.add(jsonObject1.getStr("shopName"));
                data.add(d1);
            }
            runOnUiThread(()->{
                adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,realdata);

                //设置下拉列表的风格
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                //将adapter 添加到spinner中
                spinner.setAdapter(adapter);

                //添加事件Spinner事件监听
                spinner.setOnItemSelectedListener(new SpinnerSelectedListener());
                spinner.setVisibility(View.VISIBLE);
            });
        }).start();

        Button button = findViewById(R.id.button_assessment);


            button.setOnClickListener((item)->{
                String text=textAll.getText().toString();
                if(textAll.length()>0){
                    new Thread(()->{
                        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
                        Integer id = (Integer) data.get(nowindex).get("id");
                        stringObjectHashMap.put("shopId",id);
                        stringObjectHashMap.put("context",text);
                        stringObjectHashMap.put("userid", Myconfig.getValue("user.id"));
                        System.out.println(stringObjectHashMap);
                        String body = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/quality/submitsth").form(stringObjectHashMap).execute().body();
                        System.out.println(body);
                        runOnUiThread(()->{
                            Integer data1 = new JSONObject(body).getInt("code");
                            if (data1==200){
                                Toast.makeText(QualityActivity.this, "评价成功",
                                        Toast.LENGTH_LONG).show();
                            }
                        });
                        Intent i=new Intent(QualityActivity.this, ButtomTab.class);
                        startActivity(i);
                    }).start();
                }else{
                    Toast.makeText(QualityActivity.this, "评价失败，评论内容不能为空",
                            Toast.LENGTH_LONG).show();
                }
            });
    }


    //使用数组形式操作
    class SpinnerSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                                   long arg3) {
            nowindex=arg2;
        }

        public void onNothingSelected(AdapterView<?> arg0) {
        }
    }
}