package com.llw.changeavatar.Activity.shop;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.llw.changeavatar.Adapter.HomeListAdapter;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.HomeListBean;
import com.llw.changeavatar.utils.Const;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.LinkedList;
import java.util.List;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;

public class TypeActivity extends AppCompatActivity {

    private static int count = 10;
    private static int offset = 0;
    HomeListAdapter adapter;
    RecyclerView rvRefresh;
    SmartRefreshLayout srlControl;
    View view;
    String type;
    Context context=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_shop_page);
        TextView toolbar=findViewById(R.id.toolbar_title2);
        if(getIntent().getIntExtra("type",1)==1){
            toolbar.setText("面食专区");
            type="面食";
        }
        if(getIntent().getIntExtra("type",1)==2){
            toolbar.setText("米饭专区");
            type="米饭";
        }
        if(getIntent().getIntExtra("type",1)==3){
            toolbar.setText("快餐小吃");
            type="小吃";
        }
        if(getIntent().getIntExtra("type",1)==4){
            toolbar.setText("水果饮品");
            type="饮品";
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(adapter == null || adapter.homeListData.size()==0){
                    rvRefresh = (RecyclerView)findViewById(R.id.home_recycler);
                    srlControl = (SmartRefreshLayout)findViewById(R.id.srl_control);
                    LinkedList<HomeListBean.DataBean> dataByNothing = getDataByNothing(offset, count,type);
                    if (context!=null){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                recyclerRefresh(context,dataByNothing);
                                smartRefresh();
                            }
                        });
                    }
                }
            }
        }).start();
    }
    public void recyclerRefresh(Context context, List<HomeListBean.DataBean> Data){
        adapter = new HomeListAdapter(context, Data);
        rvRefresh.setLayoutManager(new GridLayoutManager(context, 1));
        rvRefresh.setAdapter(adapter);
        rvRefresh.setNestedScrollingEnabled(false);
    }

    //监听下拉和上拉状态
    public void smartRefresh(){
        //下拉刷新
        srlControl.setOnRefreshListener(refreshlayout -> {
            srlControl.setEnableRefresh(true);//启用刷新
            /**
             * 正常来说，应该在这里加载网络数据
             * 这里我们就使用模拟数据 Data() 来模拟我们刷新出来的数据
             */
//            adapter.homeListData.clear();
            new Thread(new Runnable(){
                @Override
                public void run() {
                    LinkedList<HomeListBean.DataBean> dataByNothing = getDataByNothing(0, adapter.homeListData.size(),type);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.homeListData.clear();
                            adapter.refreshData(dataByNothing);
                        }
                    });
                }
            }).start();
            srlControl.finishRefresh();//结束刷新
        });
        //上拉加载
        srlControl.setOnLoadmoreListener(refreshlayout -> {
            srlControl.setEnableLoadmore(true);//启用加载
            /**
             * 正常来说，应该在这里加载网络数据
             * 这里我们就使用模拟数据 MoreDatas() 来模拟我们加载出来的数据
             */
            new Thread(new Runnable(){
                @Override
                public void run() {
                    LinkedList<HomeListBean.DataBean> dataByNothing = getDataByNothing(adapter.homeListData.size(), count,type);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.loadMore(dataByNothing);
                        }
                    });
                }
            }).start();
            srlControl.finishLoadmore();//结束加载
        });
    }
    LinkedList<HomeListBean.DataBean> getDataByNothing(int offset,int count,String a){
        LinkedList<HomeListBean.DataBean> res = new LinkedList<>();
        String body = HttpUtil.createGet(Const.PUBLIC_SERVER + "/api/shop/queryBytag?tag=" + a).execute().body();
        JSONArray data = new JSONObject(body).getJSONArray("data");
        int len =data.size();
        for(int i=0;i<len;i++){
            JSONObject item = data.getJSONObject(i);
            res.add(new HomeListBean.DataBean(item.getStr("shopSaleCount"),item.getStr("shopAvgTime"),item.getStr("shopUrl"),item.getInt("id"),item.getStr("shopRemark"),item.getStr("shopCate"),item.getStr("shopName")));
        }
        return res;
    }
}