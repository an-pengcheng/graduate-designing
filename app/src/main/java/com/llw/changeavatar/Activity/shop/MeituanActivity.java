package com.llw.changeavatar.Activity.shop;

import static com.llw.changeavatar.Activity.ButtomTab.activityList;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.solver.state.State;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.llw.changeavatar.R;
import com.squareup.picasso.Picasso;

public class MeituanActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private AppBarLayout appBarLayout;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private LinearLayout normal;
    private TextView phoneBuy,title,price;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_more);
        activityList.add(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        price=findViewById(R.id.tv_item_life_product_money);
        Button back=findViewById(R.id.back);
        back.setOnClickListener(view -> finish());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);
        collapsingToolbarLayout.setTitle("");
        Intent intent = getIntent();
        ImageView foodPicture= (ImageView) findViewById(R.id.my_food_name);
        Picasso.get().load(intent.getStringExtra("food_pic")).into(foodPicture);
        normal = (LinearLayout) findViewById(R.id.normal);
        phoneBuy = (TextView) findViewById(R.id.phone_buy);
        title = (TextView) findViewById(R.id.title);

        TextView foodName = (TextView) findViewById(R.id.food_name_first);
        foodName.setText(intent.getStringExtra("food_name"));
        Toolbar toolbar=findViewById(R.id.toolbar);
        appBarLayout = (AppBarLayout) findViewById(R.id.appBarLayout);
        price.setText(intent.getStringExtra("food_price"));
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                System.out.println(verticalOffset);
                if (verticalOffset == -396){//折叠
                    title.setText(intent.getStringExtra("food_name"));
                    toolbar.setBackgroundColor((Color.rgb(255,215,0)));
                    normal.setVisibility(View.GONE);
                    phoneBuy.setVisibility(View.VISIBLE);
                }else if (verticalOffset == 0){//展开
                    title.setText("");
                    toolbar.setBackgroundColor(Color.parseColor("#00000000"));
                    normal.setVisibility(View.VISIBLE);
                    phoneBuy.setVisibility(View.GONE);
                }else {//中间态
                    //collapsingToolbarLayout.setTitle("");
                    title.setText("");
                }
            }
        });
    }
}