package com.llw.changeavatar.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.llw.changeavatar.Activity.Login.QualityActivity;
import com.llw.changeavatar.Activity.TalkActivity;
import com.llw.changeavatar.Activity.shop.StoreDetailsLifeActivity;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.AssListBean;
import com.llw.changeavatar.bean.MassageListBean;
import com.llw.changeavatar.utils.Const;
import com.squareup.picasso.Picasso;

import java.util.List;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;

public class MassageListAdapter extends RecyclerView.Adapter<MassageListAdapter.ViewHolder> {
    private static final String TAG = "HomeListAdapter";
    //数据部分
    private Context mContext;//主页的上下文
    public final List<MassageListBean.DataBean> MassageListData;

    public MassageListAdapter(Context context, List<MassageListBean.DataBean> MassageList) {
        Log.d(TAG, "AssListAdapter: 创建了一个Adapter" + MassageList.size());
        mContext = context;
        MassageListData = MassageList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView massage_time;
        ImageView massage_pic;
        TextView massage_content;
        TextView massage_state;
        TextView massage_name;
        RelativeLayout clickView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            massage_name=itemView.findViewById(R.id.massage_name);
            massage_time= itemView.findViewById(R.id.massage_time);
            massage_pic = itemView.findViewById(R.id.iv_img_massage);
            massage_content = itemView.findViewById(R.id.massage_content);
            massage_state = itemView.findViewById(R.id.massage_state);
            clickView = itemView.findViewById(R.id.massage_info);
        }
    }

    //创建单项布局
    @NonNull
    @Override
    public MassageListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.massage_item, parent, false);
        return new MassageListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MassageListAdapter.ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: 填充了数据");
        //用于对RecyclerView的子项进行赋值，会在每个子项滚动到屏幕内的时候执行
        MassageListBean.DataBean MassageListData= this.MassageListData.get(position);
        holder.massage_time.setText( MassageListData.getMassage_time());
        holder.massage_state.setText(MassageListData.getMassage_state());
        if(MassageListData.getMassage_content().length()>15){
            holder.massage_content.setText(MassageListData.getMassage_content().substring(0,15)+"...");
        }else{
            holder.massage_content.setText(MassageListData.getMassage_content());
        }

        if(MassageListData.getMassage_shopid()==0){
            holder.massage_name.setText("系统通知");
        }else{
            new Thread(()->{
                String body = HttpUtil.createGet(Const.PUBLIC_SERVER + "/api/shop/queryByshopid?shopid=" + MassageListData.getMassage_shopid()).execute().body().trim();
                JSONObject data = new JSONObject(body).getJSONObject("data");
                ((Activity)mContext).runOnUiThread(()->{
                    holder.massage_name.setText(data.getStr("shopName"));
                    MassageListData.setMassage_pic(data.getStr("shopUrl"));
                    Picasso.get().load(MassageListData.getMassage_pic()).into(holder.massage_pic);
                });
            }).start();
        }

        //点击进入详情页
        holder.clickView.setOnClickListener(view -> {
            if(MassageListData.getMassage_shopid()!=0){
                Intent intent = new Intent(mContext, TalkActivity.class);
                intent.putExtra("room_id",MassageListData.getMassage_shopid());
                intent.putExtra("user_id",MassageListData.getMassage_userid());
                intent.putExtra("shop_pic",MassageListData.getMassage_pic());
                intent.putExtra("massage_content",MassageListData.getMassage_content());
                mContext.startActivity(intent);
            }else {
                Intent intent = new Intent(mContext, QualityActivity.class);
                intent.putExtra("room_id",MassageListData.getMassage_shopid());
                intent.putExtra("user_id",MassageListData.getMassage_userid());
                intent.putExtra("shop_pic",MassageListData.getMassage_pic());
                intent.putExtra("massage_content",MassageListData.getMassage_content());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return MassageListData.size();
    }

    public void loadMore(List<MassageListBean.DataBean> strings) {
        MassageListData.addAll(strings);
        notifyDataSetChanged();
    }

    public void refreshData(List<MassageListBean.DataBean> strings) {
        MassageListData.addAll(0, strings);
        notifyDataSetChanged();
    }

    public interface RefreshInterface {
        void Refresh(int ints);
    }
}
