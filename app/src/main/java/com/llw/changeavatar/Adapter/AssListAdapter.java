package com.llw.changeavatar.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.llw.changeavatar.Activity.shop.AssessmentActivity;
import com.llw.changeavatar.Activity.shop.StoreDetailsLifeActivity;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.AssListBean;
import com.llw.changeavatar.bean.HomeListBean;
import com.llw.changeavatar.bean.PurchaseListBean;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AssListAdapter extends RecyclerView.Adapter<AssListAdapter.ViewHolder> {
    private static final String TAG = "HomeListAdapter";
    //数据部分
    private Context mContext;//主页的上下文
    public final List<AssListBean.DataBean> AssListData;
    private HomeListBean.DataBean homeListData;

    public AssListAdapter(Context context,List<AssListBean.DataBean> AssList){
        Log.d(TAG, "AssListAdapter: 创建了一个Adapter"+AssList.size());
        mContext = context;
        AssListData =AssList;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView user_ass_name;
        TextView user_ass_stars;
        TextView user_ass_content;
        TextView shop_ass_name;
        ImageView user_ass_pic;
        ImageView shop_ass_pic;
        LinearLayout clickView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            user_ass_name= itemView.findViewById(R.id.user_ass_name);
            user_ass_stars= itemView.findViewById(R.id.user_ass_stars);
            user_ass_content = itemView.findViewById(R.id.user_ass_content);
            shop_ass_name=itemView.findViewById(R.id.shop_ass_name);
            user_ass_pic = itemView.findViewById(R.id.user_ass_pic);
            shop_ass_pic = itemView.findViewById(R.id.shop_ass_pic);
            clickView = itemView.findViewById(R.id.ass_list_item_click);
        }
    }
    //创建单项布局
    @NonNull
    @Override
    public AssListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.assessment_item,parent,false);
        return new AssListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AssListAdapter.ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: 填充了数据");
        //用于对RecyclerView的子项进行赋值，会在每个子项滚动到屏幕内的时候执行
        AssListBean.DataBean AssListData= this.AssListData.get(position);
        holder.user_ass_name.setText(AssListData.getUser_ass_name());
        holder.user_ass_stars.setText("综合评价："+(AssListData.getStar1()+AssListData.getStar2()+AssListData.getStar3())+"星");
        holder.user_ass_content.setText(AssListData.getUser_ass_content().split("#")[0]);
        holder.shop_ass_name.setText(AssListData.getShop_ass_name());
        Picasso.get().load(AssListData.getUser_ass_content().split("#")[1]).into(holder.user_ass_pic);
        Picasso.get().load(AssListData.getShop_ass_pic()).into(holder.shop_ass_pic);

        //点击进入详情页
        holder.clickView.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, StoreDetailsLifeActivity.class);
            intent.putExtra("id",AssListData.getShop_id());
            intent.putExtra("title",AssListData.getShop_ass_name());
            intent.putExtra("usercontent",AssListData.getUser_ass_content());
            intent.putExtra("username",AssListData.getUser_ass_name());
            intent.putExtra("userstars","综合："+AssListData.getStar1()+"口味："+AssListData.getStar2()+"速度："+AssListData.getStar3());
            intent.putExtra("url",AssListData.getUser_ass_pic());
            intent.putExtra("user",AssListData.getUser_ass_name());
            intent.putExtra("assessment",AssListData.getStar1()+AssListData.getStar2()+AssListData.getStar3());
            mContext.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return AssListData.size();
    }

    public void loadMore(List<AssListBean.DataBean> strings) {
        AssListData.addAll(strings);
        notifyDataSetChanged();
    }
    public void refreshData(List<AssListBean.DataBean> strings) {
        AssListData.addAll(0, strings);
        notifyDataSetChanged();
    }
    public interface RefreshInterface{
        void Refresh(int ints);
    }
}

