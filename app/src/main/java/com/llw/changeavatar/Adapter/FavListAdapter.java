package com.llw.changeavatar.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.llw.changeavatar.Activity.shop.StoreDetailsLifeActivity;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.AssListBean;
import com.llw.changeavatar.bean.FavoriteListBean;
import com.llw.changeavatar.bean.HomeListBean;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FavListAdapter  extends RecyclerView.Adapter<FavListAdapter.ViewHolder>{
    private static final String TAG = "FavListAdapter";
    //数据部分
    private Context mContext;//主页的上下文
    public final List<FavoriteListBean.DataBean> FavListData;

    public FavListAdapter(Context context,List<FavoriteListBean.DataBean> AssList){
        Log.d(TAG, "HomeListAdapter: 创建了一个Adapter");
        mContext = context;
        FavListData =AssList;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView fav_shop_name;
        TextView fav_shop_type;
        TextView fav_shop_place;
        ImageView fav_shop_pic;
        LinearLayout clickView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            fav_shop_name= itemView.findViewById(R.id.fav_list_item_title);
            fav_shop_type= itemView.findViewById(R.id.fav_list_item_deliver);
            fav_shop_place = itemView.findViewById(R.id.fav_list_item_place);
            fav_shop_pic = itemView.findViewById(R.id.fav_list_item_image);
            clickView = itemView.findViewById(R.id.fav_list_item_click);
        }
    }
    //创建单项布局
    @NonNull
    @Override
    public FavListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.favorites_item,parent,false);
        return new FavListAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull FavListAdapter.ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: 填充了数据");
        //用于对RecyclerView的子项进行赋值，会在每个子项滚动到屏幕内的时候执行
        FavoriteListBean.DataBean FavListData= this.FavListData.get(position);
        holder.fav_shop_name.setText(FavListData.getFav_shop_name());
        holder.fav_shop_type.setText(FavListData.getFav_shop_type());
        holder.fav_shop_place.setText(FavListData.getFav_shop_main());
        Picasso.get().load(FavListData.getFav_shop_pic()).into(holder.fav_shop_pic);

        //点击进入详情页
        holder.clickView.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, StoreDetailsLifeActivity.class);
            intent.putExtra("title",FavListData.getFav_shop_name());
            intent.putExtra("url",FavListData.getFav_shop_pic());
            intent.putExtra("user_id",FavListData.getFav_user_id());
            intent.putExtra("shop_id",FavListData.getFav_shop_id());
            intent.putExtra("id",FavListData.getFav_shop_id());
            intent.putExtra("deliver",FavListData.getFav_shop_place());
            intent.putExtra("salemain",FavListData.getFav_shop_main());
            mContext.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return FavListData.size();
    }

    public void loadMore(List<FavoriteListBean.DataBean> strings) {
        FavListData.addAll(strings);
        notifyDataSetChanged();
    }
    public void refreshData(List<FavoriteListBean.DataBean> strings) {
        FavListData.addAll(0, strings);
        notifyDataSetChanged();
    }
    public interface RefreshInterface{
        void Refresh(int ints);
    }
}


