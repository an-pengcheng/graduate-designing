package com.llw.changeavatar.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.llw.changeavatar.Activity.TalkActivity;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.ShopAssListBean;
import com.llw.changeavatar.utils.Const;
import com.squareup.picasso.Picasso;

import java.util.List;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;

public class ShopAssListAdapter extends RecyclerView.Adapter<ShopAssListAdapter.ViewHolder>{
    private static final String TAG = "HomeListAdapter";
    //数据部分
    private Context mContext;//主页的上下文
    public final List<ShopAssListBean.DataBean> ShopAssListData;

    public ShopAssListAdapter(Context context, List<ShopAssListBean.DataBean> ShopAssList) {
        Log.d(TAG, "<ShopAssListAdapter: 创建了一个Adapter" + ShopAssList.size());
        mContext = context;
        ShopAssListData = ShopAssList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView shop_ass_time;
        ImageView shop_ass_user_pic;
        TextView shop_user_ass_content;
        TextView shop_user_ass_stars;
        TextView shop_ass_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            shop_ass_name=itemView.findViewById(R.id.shop_ass_name);
            shop_ass_user_pic = itemView.findViewById(R.id.shop_ass_user_pic);
            shop_user_ass_content = itemView.findViewById(R.id.shop_user_ass_content);
            shop_user_ass_stars = itemView.findViewById(R.id.shop_user_ass_stars);
        }
    }

    //创建单项布局
    @NonNull
    @Override
    public ShopAssListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.detail_item, parent, false);
        return new ShopAssListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: 填充了数据");
        //用于对RecyclerView的子项进行赋值，会在每个子项滚动到屏幕内的时候执行
        ShopAssListBean.DataBean ShopAssListData= this.ShopAssListData.get(position);
        holder.shop_ass_name.setText( ShopAssListData.getShop_ass_name());
        holder.shop_user_ass_stars.setText(ShopAssListData.getShop_user_ass_stars());
        holder.shop_user_ass_content.setText(ShopAssListData.getShop_user_ass_content());
        new Thread(()->{
            String body = HttpUtil.createGet(Const.PUBLIC_SERVER + "/api/user/getinfo?id=" + ShopAssListData.getShop_ass_user_id()).execute().body().trim();
            JSONObject data = new JSONObject(body).getJSONObject("data");
            ((Activity)mContext).runOnUiThread(()->{
                if(ShopAssListData.getShop_ass_user_pic()!=null) {
                    Picasso.get().load(ShopAssListData.getShop_ass_user_pic()).into(holder.shop_ass_user_pic);
                }else {
                    Picasso.get().load("https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png").into(holder.shop_ass_user_pic);
                }
            });
        }).start();
    }


    @Override
    public int getItemCount() {
        return ShopAssListData.size();
    }

    public void loadMore(List<ShopAssListBean.DataBean> strings) {
        ShopAssListData.addAll(strings);
        notifyDataSetChanged();
    }

    public void refreshData(List<ShopAssListBean.DataBean> strings) {
        ShopAssListData.addAll(0, strings);
        notifyDataSetChanged();
    }

    public interface RefreshInterface {
        void Refresh(int ints);
    }
}

