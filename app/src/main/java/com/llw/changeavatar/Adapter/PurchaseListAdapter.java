package com.llw.changeavatar.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.llw.changeavatar.Activity.shop.OrderActivity;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.BuyListBean;
import com.llw.changeavatar.bean.PurchaseListBean;
import com.squareup.picasso.Picasso;

import java.util.List;

import cn.hutool.core.util.StrUtil;

public class PurchaseListAdapter extends RecyclerView.Adapter<PurchaseListAdapter.ViewHolder> {
    private static final String TAG = "HomeListAdapter";
    //数据部分
    private Context mContext;//主页的上下文
    public final List<PurchaseListBean.DataBean> PurchaseListData;
    public PurchaseListAdapter(Context context,List<PurchaseListBean.DataBean> foodList){
        Log.d(TAG, "HomeListAdapter: 创建了一个Adapter");
        mContext = context;
        PurchaseListData =foodList;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView order_comment;
        TextView order_price;
        TextView order_shop_name;
        ImageView order_food_pic;
        TextView order_count;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            order_comment= itemView.findViewById(R.id.order_list_comment_one);
            order_price = itemView.findViewById(R.id.order_list_price_one);
            order_shop_name = itemView.findViewById(R.id.order_list_name);
            order_food_pic=itemView.findViewById(R.id.order_list_pic_one);
            order_count = itemView.findViewById(R.id.order_list_number_one);
        }
    }
    @Override
    public int getItemCount() {
        return PurchaseListData.size();
    }
    //创建单项布局
    @NonNull
    @Override
    public PurchaseListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list_items,parent,false);
        return new PurchaseListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PurchaseListAdapter.ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: 填充了数据");
        //用于对RecyclerView的子项进行赋值，会在每个子项滚动到屏幕内的时候执行
        PurchaseListBean.DataBean PurchaseListData= this.PurchaseListData.get(position);
        holder.order_price.setText("$"+PurchaseListData.getNowprice()/100.0 );
        holder.order_shop_name.setText(PurchaseListData.getName());
        holder.order_count.setText("X"+PurchaseListData.getCount());
        if(StrUtil.isNotBlank(PurchaseListData.getItem_url())){
            Picasso.get().load(PurchaseListData.getItem_url()).into(holder.order_food_pic);
        }else{
            Picasso.get().load("https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.pnghttps://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png").into(holder.order_food_pic);
        }
//        holder.order_list_item_click.setOnClickListener(view -> {
//            Intent intent = new Intent(mContext, OrderActivity.class);
//            mContext.startActivity(intent);
//        });
    }
}
