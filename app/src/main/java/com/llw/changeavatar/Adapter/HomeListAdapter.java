package com.llw.changeavatar.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.llw.changeavatar.Activity.shop.StoreDetailsLifeActivity;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.HomeListBean;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomeListAdapter extends RecyclerView.Adapter<HomeListAdapter.ViewHolder> {
    private static final String TAG = "HomeListAdapter";
    //数据部分
    private Context mContext;//主页的上下文
    public final List<HomeListBean.DataBean> homeListData;
    public HomeListAdapter(Context context,List<HomeListBean.DataBean> fruitList){
        Log.d(TAG, "HomeListAdapter: 创建了一个Adapter");
        mContext = context;
        homeListData =fruitList;
    }
    //布局元素实例化
    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView titleText;
        TextView saleText;
        TextView deliverText;
        TextView deliverTimeText;
        TextView lableText;
        ImageView previewImage;
        LinearLayout clickView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            titleText = itemView.findViewById(R.id.home_list_item_title);
            saleText = itemView.findViewById(R.id.home_list_item_sale);
            deliverText = itemView.findViewById(R.id.home_list_item_deliver);
            deliverTimeText = itemView.findViewById(R.id.home_list_item_deliver_time);
            lableText = itemView.findViewById(R.id.home_list_item_lable);
            previewImage = itemView.findViewById(R.id.home_list_item_image);
            clickView = itemView.findViewById(R.id.home_list_item_click);
        }
    }
    //创建单项布局
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.home_item,parent,false);
        return new ViewHolder(view);
    }
    //用数据填充单项布局
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: 填充了数据");
        //用于对RecyclerView的子项进行赋值，会在每个子项滚动到屏幕内的时候执行
        HomeListBean.DataBean homeListData= this.homeListData.get(position);
        holder.titleText.setText(homeListData.getTitle());
        holder.saleText.setText("主营："+homeListData.getSale());
        holder.deliverText.setText("总销量："+homeListData.getDeliver()+"单");
        if(homeListData.getDeliver()==null) holder.deliverText.setText("总销量："+"0单");
        if(homeListData.getDeliverTime().equals("1")){
            holder.deliverTimeText.setText("空闲");
            holder.deliverTimeText.setTextColor(Color.rgb(0,255,0));
            holder.deliverTimeText.setBackgroundColor(Color.rgb(202 ,255 ,112));
        }
        if(homeListData.getDeliverTime().equals("2")){
            holder.deliverTimeText.setText("忙碌");
            holder.deliverTimeText.setTextColor(Color.rgb(255 ,215, 0));
            holder.deliverTimeText.setBackgroundColor(Color.rgb(255, 255 ,224));
        }
        if(homeListData.getDeliverTime().equals("3")){
            holder.deliverTimeText.setText("拥挤");
            holder.deliverTimeText.setTextColor(Color.rgb(238 ,99 ,99));
            holder.deliverTimeText.setBackgroundColor(Color.rgb(255,160,122));
        }
        if(homeListData.getDeliverTime().equals("4")){
            holder.deliverTimeText.setText("爆满");
            holder.deliverTimeText.setTextColor(Color.rgb(145, 44 ,238));
            holder.deliverTimeText.setBackgroundColor(Color.rgb(238 ,130 ,238));
        }
        holder.lableText.setText(homeListData.getLable());
        Picasso.get().load(homeListData.getDetailUrl()).into(holder.previewImage);

        //点击进入详情页
        holder.clickView.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, StoreDetailsLifeActivity.class);
            intent.putExtra("title",homeListData.getTitle());
            intent.putExtra("url",homeListData.getDetailUrl());
            intent.putExtra("deliver",homeListData.getDeliverTime());
            intent.putExtra("id",homeListData.getId());
            intent.putExtra("salecount",homeListData.getDeliver());
            intent.putExtra("salemain",homeListData.getSale());
            mContext.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return homeListData.size();
    }

    public void loadMore(List<HomeListBean.DataBean> strings) {
        homeListData.addAll(strings);
        notifyDataSetChanged();
    }
    public void refreshData(List<HomeListBean.DataBean> strings) {
        homeListData.addAll(0, strings);
        notifyDataSetChanged();
    }
    public interface RefreshInterface{
        void Refresh(int ints);
    }
}


