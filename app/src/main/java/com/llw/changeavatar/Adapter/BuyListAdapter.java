package com.llw.changeavatar.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.llw.changeavatar.Activity.shop.AssessmentActivity;
import com.llw.changeavatar.Activity.shop.OrderActivity;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.BuyListBean;
import com.llw.changeavatar.bean.HomeListBean;
import com.llw.changeavatar.utils.Const;
import com.squareup.picasso.Picasso;

import java.util.List;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;

public class BuyListAdapter extends RecyclerView.Adapter<BuyListAdapter.ViewHolder>{
    private static final String TAG = "HomeListAdapter";
    //数据部分
    private Context mContext;//主页的上下文
    public final List<BuyListBean.DataBean> BuyListData;
    public BuyListAdapter(Context context,List<BuyListBean.DataBean> foodList){
        Log.d(TAG, "HomeListAdapter: 创建了一个Adapter");
        mContext = context;
        BuyListData =foodList;
    }
    //布局元素实例化
    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView order_comment;
        TextView order_price;
        TextView order_foods;
        TextView order_status;
        TextView order_shop_name;
        ImageView order_shop_pic;
        LinearLayout order_list_item_click;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            order_comment= itemView.findViewById(R.id.order_comment);
            order_price = itemView.findViewById(R.id.order_price);
            order_foods = itemView.findViewById(R.id.order_foods);
            order_status = itemView.findViewById(R.id.order_status);
            order_shop_name = itemView.findViewById(R.id.order_shop_name);
            order_shop_pic = itemView.findViewById(R.id.order_shop_pic);
            order_list_item_click = itemView.findViewById(R.id.order_list_item_click);
        }
    }
    //创建单项布局
    @NonNull
    @Override
    public BuyListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: 填充了数据");
        //用于对RecyclerView的子项进行赋值，会在每个子项滚动到屏幕内的时候执行
        BuyListBean.DataBean BuyListData= this.BuyListData.get(position);
        holder.order_price.setText("$"+Integer.valueOf( BuyListData.getOrder_price())/100.0);
        holder.order_foods.setText(BuyListData.getOrder_item());
        if(BuyListData.getIspay()!=0){
            holder.order_status.setText("未支付");
            holder.order_comment.setText("立即付款");
        }else{
            holder.order_status.setText("订单已完成");
            holder.order_comment.setText("立即评价");
        }
        holder.order_shop_name.setText(BuyListData.getOrder_name());
        new Thread(()->{
            String body = HttpUtil.createGet(Const.PUBLIC_SERVER + "/api/shop/queryByshopid?shopid=" + BuyListData.getShop_id()).execute().body().trim();
            JSONObject data = new JSONObject(body).getJSONObject("data");
            BuyListData.setPic_url(data.getStr("shopUrl"));
//            BuyListData.setShop_id(data.getInt("id"));
            ((Activity)mContext).runOnUiThread(()->{
                Picasso.get().load(BuyListData.getPic_url()).into(holder.order_shop_pic);
            });
        }).start();
        if(BuyListData.getIspay()!=0){
            holder.order_comment.setOnClickListener(view -> {
                Intent intent = new Intent(mContext, OrderActivity.class);
                intent.putExtra("shop_name",BuyListData.getShop_name());
                intent.putExtra("pic_url",BuyListData.getPic_url());
                intent.putExtra("order_name",BuyListData.getOrder_name());
                intent.putExtra("id",BuyListData.getId());
                intent.putExtra("order_item",BuyListData.getOrder_item());
                intent.putExtra("order_items",BuyListData.getOrder_items());
                intent.putExtra("ispay",BuyListData.getIspay());
                intent.putExtra("order_price",BuyListData.getOrder_price());
                intent.putExtra("shop_id",BuyListData.getShop_id());
                intent.putExtra("user_id",BuyListData.getUser_id());
                mContext.startActivity(intent);
            });
        }else {
            holder.order_comment.setOnClickListener(view -> {
                Intent intent = new Intent(mContext, AssessmentActivity.class);
                intent.putExtra("shop_name",BuyListData.getShop_name());
                intent.putExtra("pic_url",BuyListData.getPic_url());
                intent.putExtra("order_name",BuyListData.getOrder_name());
                intent.putExtra("id",BuyListData.getId());
                intent.putExtra("order_item",BuyListData.getOrder_item());
                intent.putExtra("order_items",BuyListData.getOrder_items());
                intent.putExtra("ispay",BuyListData.getIspay());
                intent.putExtra("order_price",BuyListData.getOrder_price());
                intent.putExtra("shop_id",BuyListData.getShop_id());
                intent.putExtra("user_id",BuyListData.getUser_id());
                mContext.startActivity(intent);
            });
        }
    }

    @Override
    public int getItemCount() {
        return BuyListData.size();
    }
    public void loadMore(List<BuyListBean.DataBean> strings) {
        BuyListData.addAll(strings);
        notifyDataSetChanged();
    }
    public void refreshData(List<BuyListBean.DataBean> strings) {
        BuyListData.addAll(0, strings);
        notifyDataSetChanged();
    }
    public interface RefreshInterface {
        void Refresh(int ints);
    }
}
