package com.llw.changeavatar.Fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.llw.changeavatar.R;

public class DataGenerator {

    public static final int []mTabRes = new int[]{R.drawable.icons8_basil_1,R.drawable.icons8_grocery_bag_1,R.drawable.icons8_bento_1,R.drawable.icons8_bread_1};
    public static final int []mTabResPressed = new int[]{R.drawable.icons8_basil,R.drawable.icons8_grocery_bag,R.drawable.icons8_bento,R.drawable.icons8_bread};
    public static final String []mTabTitle = new String[]{"首页","商店","订单","我的"};

    public static Fragment[] getFragments(){
        Fragment fragments[] = new Fragment[4];
        fragments[0] = new HomePageFragment();
        fragments[1] = new ShopPageFragment();
        fragments[2] = new MassagePageFragment();
        fragments[3] = new UserPageFragment();
        return fragments;
    }

    // 获取了position的视图view，并初始化里面的元素，用于init
    public static View getTabView(Context context, int position){
        View view = LayoutInflater.from(context).inflate(R.layout.page_style,null);
        ImageView tabIcon = view.findViewById(R.id.tab_content_image);
        tabIcon.setImageResource(DataGenerator.mTabRes[position]);
        TextView tabText = view.findViewById(R.id.tab_content_text);
        tabText.setText(mTabTitle[position]);
        return view;
    }
}