package com.llw.changeavatar.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.llw.changeavatar.Activity.ButtomTab;
import com.llw.changeavatar.Activity.Login.MainActivity;
import com.llw.changeavatar.Activity.Login.MassageActivity;
import com.llw.changeavatar.Activity.shop.TypeActivity;
import com.llw.changeavatar.Adapter.LoopViewAdapter;
import com.llw.changeavatar.R;
import com.llw.changeavatar.pager1OnClickLister;
import com.llw.changeavatar.pagerOnClickListener;
import com.llw.changeavatar.utils.Const;
import com.llw.changeavatar.utils.Myconfig;
import com.squareup.picasso.Picasso;


import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.hutool.core.map.MapUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomePageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomePageFragment extends Fragment {
    private LinearLayout mGallery,mGallery1;
    private int[] mImgIds, MImgIds;
    private LayoutInflater mInflater;
    View view;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    FloatingActionButton fab;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private ViewPager viewPager;  //轮播图模块
    private int[] mImg;
    private int[] mImg_id;
    private int[] mImg_id2;
    private int[] mImg_id1;

    private String[] mDec;
    private String[] mDec1;
    private String[] mDec2;
    private ArrayList<ImageView> mImgList;
    private ArrayList<ImageView> mImgList1 = new ArrayList<ImageView>();
    private ArrayList<ImageView> mImgList2= new ArrayList<ImageView>();;
    private LinearLayout ll_dots_container,text1,text2,text3,text4;
    private TextView loop_dec;
    private int previousSelectedPosition = 0;
    boolean isRunning = false;

    public HomePageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
        initView();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomePageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomePageFragment newInstance(String param1, String param2) {
        HomePageFragment fragment = new HomePageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_page, container, false);
        Myconfig.init(view.getContext());
        new Thread(() -> {
            if (Myconfig.getValue("user.username").length() > 0 && Myconfig.getValue("user.userpwd").length() > 0) {
                Map<String, String> map = MapUtil.builder(new HashMap<String, String>()).
                        put("username", Myconfig.getValue("user.username")).
                        put("userpwd", Myconfig.getValue("user.userpwd")).build();
            }
        }).start();

        viewPager = view.findViewById(R.id.loopviewpager);
        ll_dots_container = view.findViewById(R.id.ll_dots_loop);
        text1=view.findViewById(R.id.text1);
        text2 = view.findViewById(R.id.text2);
        text3=view.findViewById(R.id.text3);
        text4=view.findViewById(R.id.text4);
        loop_dec = view.findViewById(R.id.loop_dec);
        initLoopView();
        mInflater = LayoutInflater.from(getContext());
        text1.setOnClickListener(view1 -> {
            Intent i = new Intent(view1.getContext(), TypeActivity.class);
            i.putExtra("type",1);
            startActivity(i);
        });
        text2.setOnClickListener(view2 -> {
            Intent i = new Intent(view2.getContext(), TypeActivity.class);
            i.putExtra("type",2);
            startActivity(i);
        });
        text3.setOnClickListener(view3 -> {
            Intent i = new Intent(view3.getContext(), TypeActivity.class);
            i.putExtra("type",3);
            startActivity(i);
        });
        text4.setOnClickListener(view4 -> {
            Intent i = new Intent(view4.getContext(), TypeActivity.class);
            i.putExtra("type",4);
            startActivity(i);
        });

        initData();
        initView();


        return view;
    }


    @SuppressLint("CutPasteId")
    private void initData() {
        mImgIds = new int[]{R.drawable.bestshop, R.drawable.bestshop, R.drawable.bestshop, R.drawable.bestshop,
                R.drawable.bestshop, R.drawable.bestshop, R.drawable.bestshop,R.drawable.bestshop,R.drawable.bestshop,R.drawable.bestshop,R.drawable.bestshop,
        };
        MImgIds = new int[]{R.drawable.icons8_basil, R.drawable.icons8_basil, R.drawable.icons8_basil, R.drawable.icons8_basil,
                R.drawable.icons8_basil, R.drawable.icons8_basil, R.drawable.icons8_basil,R.drawable.icons8_basil,R.drawable.icons8_basil,R.drawable.icons8_basil,R.drawable.icons8_basil,
        };
        mImg_id1 =new int[]{R.id.pager_img6,R.id.pager_img7,R.id.pager_img8,R.id.pager_img9,R.id.pager_img10,R.id.pager_img11,R.id.pager_img12,R.id.pager_img13,R.id.pager_img14,R.id.pager_img15,R.id.pager_img16,R.id.pager_img17,R.id.pager_img17,R.id.pager_img18,R.id.pager_img19,R.id.pager_img20,R.id.pager_img21,R.id.pager_img22,R.id.pager_img23,R.id.pager_img24,R.id.pager_img25,
        };
        //mImgList1
        ImageView imageView;
        for (int i = 0; i < mImgIds.length; i++) {
            //初始化要显示的图片对象
            imageView = new ImageView(getContext());
            imageView.setBackgroundResource(mImgIds[i]);
            imageView.setId(mImg_id1[i]);
            imageView.setOnClickListener(new pagerOnClickListener(getContext()));
            mImgList1.add(imageView);
        }
        mDec1 = new String[]{
                "a", "b", "c", "d", "e","f","g","h","i","j",
        };
        mDec2 = new String[]{
                "a", "b", "c", "d", "e","f","g","h","i","j",
        };
        new Thread(() -> {
            String string = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/activity/bestfoodBycount").execute().body();
            JSONObject jsonObject = new JSONObject(string);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            mGallery1 = (LinearLayout) view.findViewById(R.id.id_gallery_sale);

            String string1 = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/activity/bestfoodByshop").execute().body();
            JSONObject jsonObject3 = new JSONObject(string1);
            JSONArray jsonArray1 = jsonObject3.getJSONArray("data");
            mGallery = (LinearLayout) view.findViewById(R.id.id_gallery);
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String img = jsonObject1.getStr("item_url");
                int shopid = jsonObject1.getInt("shopid");
                mDec2[i] = jsonObject1.getStr("name");
                String body = HttpUtil.createGet(Const.PUBLIC_SERVER + "/api/shop/queryByshopid?shopid=" + shopid).execute().body().trim();
                JSONObject data = new JSONObject(body).getJSONObject("data");
                int finalI = i;
                if(getActivity()!=null)
                getActivity().runOnUiThread(() -> {
                        View view = mInflater.inflate(R.layout.hotsale_item,
                                mGallery1, false);
                    TextView shopname = (TextView) view.findViewById(R.id.id_index_gallery_item_text_status_sale);
                    TextView shopcount = (TextView) view.findViewById(R.id.id_index_gallery_item_text_salecount_sale);
                    shopname.setText(data.getStr("shopName"));
                    shopcount.setText(data.getStr("shopSaleCount")+"人付款");
                        ImageView img2 = (ImageView) view.findViewById(R.id.id_index_gallery_item_image_sale);
                        Picasso.get().load(img).into(img2);
                        img2.setImageResource(MImgIds[finalI]);
                        TextView txt = (TextView) view.findViewById(R.id.id_index_gallery_item_text_name_sale);
                        txt.setText(mDec2[finalI]);
                        txt.setTextColor(Color.BLACK);
                    img2.setId(mImg_id1[finalI+jsonArray1.size()-1]);
                    if(getActivity()!=null){
                        Intent ip=getActivity().getIntent();
                        ip.putExtra("title",data.getStr("shopName"));
                        ip.putExtra("url",data.getStr("shopUrl"));
                        ip.putExtra("deliver",data.getStr("shopAvgTime"));
                        ip.putExtra("saleconunt",data.getStr("shopSaleCount"));
                        ip.putExtra("salemain",data.getStr("shopCate"));
                        ip.putExtra("id",data.getInt("id"));

                        img2.setOnClickListener(new pager1OnClickLister(getContext(),ip));
                        mGallery1.addView(view);
                    }


                });
            }
            for (int i = 0; i < jsonArray1.size(); i++) {
                JSONObject jsonObject2 = jsonArray1.getJSONObject(i);
                String img = jsonObject2.getStr("item_url");
                String shopid = jsonObject2.getStr("shopid");
                mDec1[i] = jsonObject2.getStr("name");
                int finalI1 = i;
                String body1 = HttpUtil.createGet(Const.PUBLIC_SERVER + "/api/shop/queryByshopid?shopid=" + shopid).execute().body().trim();
                JSONObject data = new JSONObject(body1).getJSONObject("data");
                if(getActivity()!=null)
                getActivity().runOnUiThread(() -> {
                    View view = mInflater.inflate(R.layout.recommend_item,
                            mGallery, false);
                    ImageView img3 = (ImageView) view.findViewById(R.id.id_index_gallery_item_image);
                    Picasso.get().load(img).into(img3);
                    img3.setImageResource(mImgIds[finalI1]);
                    TextView txt = (TextView) view.findViewById(R.id.id_index_gallery_item_text);
                    txt.setText(mDec1[finalI1]);
                    txt.setTextColor(Color.BLACK);
                    img3.setId(mImg_id1[finalI1]);
                    if(getActivity()!=null){
                        Intent ip=getActivity().getIntent();
                        ip.putExtra("title",data.getStr("shopName"));
                        ip.putExtra("url",data.getStr("shopUrl"));
                        ip.putExtra("deliver",data.getStr("shopAvgTime"));
                        ip.putExtra("saleconunt",data.getStr("shopSaleCount"));
                        ip.putExtra("salemain",data.getStr("shopCate"));
                        ip.putExtra("id",data.getInt("id"));
                        img3.setOnClickListener(new pager1OnClickLister(getContext(),ip));
                        mGallery.addView(view);
                    }

                });
            }
        }).start();

        mGallery = (LinearLayout) view.findViewById(R.id.id_gallery_sale);

    }

    private void initView() {

    }

    public void initLoopView() {
        mImg = new int[]{
                R.drawable.bestshop,
                R.drawable.bestshop,
                R.drawable.bestshop,
                R.drawable.bestshop,
                R.drawable.bestshop,
        };

//             文本描述
        mDec = new String[]{
                "a", "b", "c", "d", "e",
        };
        new Thread(() -> {
            String string = HttpUtil.createPost(Const.PUBLIC_SERVER + "/api/activity/gethot").execute().body();
            JSONObject jsonObject = new JSONObject(string);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            mImg = new int[jsonArray.size()];
            mDec = new String[jsonArray.size()];
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String img = jsonObject1.getStr("shopUrl");
                mDec[i] = jsonObject1.getStr("shopName");
                int finalI = i;
                if(getActivity()!=null)
                getActivity().runOnUiThread(() -> {
                    Picasso.get().load(img).into(mImgList.get(finalI));
                    mImgList.get(finalI).setBackgroundColor(Color.parseColor("#ffffff"));
                });
            }
        }).start();


        mImg_id = new int[]{
                R.id.pager_img1,
                R.id.pager_img2,
                R.id.pager_img3,
                R.id.pager_img4,
                R.id.pager_img5
        };

        // 初始化要展示的5个ImageView
        mImgList = new ArrayList<ImageView>();
        ImageView imageView;
        View dotView;
        LinearLayout.LayoutParams layoutParams;
        for (int i = 0; i < mImg.length; i++) {
            //初始化要显示的图片对象
            imageView = new ImageView(getContext());
            imageView.setBackgroundResource(mImg[i]);
            imageView.setId(mImg_id[i]);
            imageView.setOnClickListener(new pagerOnClickListener(getContext()));
            mImgList.add(imageView);
            //加引导点
            dotView = new View(getContext());
            dotView.setBackgroundResource(R.drawable.dot);
            layoutParams = new LinearLayout.LayoutParams(10, 10);
            if (i != 0) {
                layoutParams.leftMargin = 10;
            }
            //设置默认所有都不可用
            dotView.setEnabled(false);
            ll_dots_container.addView(dotView, layoutParams);
        }

        ll_dots_container.getChildAt(0).setEnabled(true);
        loop_dec.setText(mDec[0]);
        previousSelectedPosition = 0;
        //设置适配器
        viewPager.setAdapter(new LoopViewAdapter(mImgList));
        viewPager.setOffscreenPageLimit(2);
        // 把ViewPager设置为默认选中Integer.MAX_VALUE / t2，从十几亿次开始轮播图片，达到无限循环目的;
        int m = (Integer.MAX_VALUE / 2) % mImgList.size();
        int currentPosition = Integer.MAX_VALUE / 2 - m;
        viewPager.setCurrentItem(currentPosition);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                int newPosition = i % mImgList.size();
                loop_dec.setText(mDec[newPosition]);
                ll_dots_container.getChildAt(previousSelectedPosition).setEnabled(false);
                ll_dots_container.getChildAt(newPosition).setEnabled(true);
                previousSelectedPosition = newPosition;
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        // 开启轮询
        new Thread() {
            public void run() {
                isRunning = true;
                while (isRunning) {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //下一条
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                            }
                        });
                    }

                }
            }
        }.start();

    }

}