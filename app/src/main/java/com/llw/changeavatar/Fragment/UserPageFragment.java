package com.llw.changeavatar.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.imageview.ShapeableImageView;
import com.llw.changeavatar.Activity.Login.AssListActivity;
import com.llw.changeavatar.Activity.Login.FavoritesActivity;
import com.llw.changeavatar.Activity.Login.MainActivity;
import com.llw.changeavatar.Activity.Login.MassageActivity;
import com.llw.changeavatar.R;
import com.llw.changeavatar.Activity.Login.UserInfo;
import com.llw.changeavatar.utils.Myconfig;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserPageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserPageFragment extends Fragment {
    TextView textView_login;
    TextView textView_name;
    ShapeableImageView viewById;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public UserPageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserPageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserPageFragment newInstance(String param1, String param2) {
        UserPageFragment fragment = new UserPageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(Myconfig.getValue("user.headpic").length()>0){
            Picasso.get().load(Myconfig.getValue("user.headpic")).into(viewById);
        }else{
            Picasso.get().load("https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png").into(viewById);
        }
        if(Myconfig.getValue("user.nickname").length()>0){
            textView_login.setText(Myconfig.getValue("user.nickname"));
        }else {
            textView_login.setText("请先登录");
        }
        if(Myconfig.getValue("user.username").length()>0){
            textView_name.setText("账号："+Myconfig.getValue("user.username"));
        }else{
            textView_name.setText("账号：");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_user_page, container, false);
        Button btn = view.findViewById(R.id.change_info);
        textView_login=view.findViewById(R.id.my_textView);
        textView_name=view.findViewById(R.id.textView2);
        viewById = (ShapeableImageView)view.findViewById(R.id.iv_img_change);
        LinearLayout login_page=view.findViewById(R.id.login_page);
        LinearLayout userMMore = view.findViewById(R.id.Usermore);
        LinearLayout myFavorite = view.findViewById(R.id.my_favorite);
        LinearLayout myMessages = view.findViewById(R.id.my_massages);
        LinearLayout userAssessment = view.findViewById(R.id.userassessment);
        myMessages.setOnClickListener(view5 -> {
            if(Myconfig.getValue("user.id").length()==0){
                startActivity(new Intent(getContext(), MainActivity.class));
            }else{
                Intent i = new Intent(view5.getContext(), MassageActivity.class);
                startActivity(i);
            }

        });
        myFavorite.setOnClickListener(view1 -> {
            if(Myconfig.getValue("user.id").length()==0){
                startActivity(new Intent(getContext(), MainActivity.class));
            }else{
                Intent i = new Intent(view1.getContext(), FavoritesActivity.class);
                startActivity(i);
            }

        });
        userAssessment.setOnClickListener(view2 -> {
            if(Myconfig.getValue("user.id").length()==0){
                startActivity(new Intent(getContext(), MainActivity.class));
            }else{
                Intent i = new Intent(view2.getContext(), AssListActivity.class);
                startActivity(i);
            }

        });
        userMMore.setOnClickListener(view3 -> {
            if(Myconfig.getValue("user.id").length()==0){
                startActivity(new Intent(getContext(), MainActivity.class));
            }else{
                Intent i = new Intent(view3.getContext(), UserInfo.class);
                startActivity(i);
            }

        });
        login_page.setOnClickListener(view4 -> {
            Intent i = new Intent(view4.getContext(), MainActivity.class);
            startActivity(i);
        });
        Myconfig.init(getContext());
        if(Myconfig.getValue("user.haedpic").length()>0){
            Picasso.get().load(Myconfig.getValue("user.haedpic")).into(viewById);
        }else{
            Picasso.get().load("https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png").into(viewById);
        }
        if(Myconfig.getValue("user.nickname").length()>0){
            textView_login.setText(Myconfig.getValue("user.nickname"));
        }else {
            textView_login.setText("请先登录");
        }
        if(Myconfig.getValue("user.username").length()>0){
            textView_name.setText("账号："+Myconfig.getValue("user.username"));
        }else{
            textView_name.setText("账号：");
        }

        return view;
    }
}