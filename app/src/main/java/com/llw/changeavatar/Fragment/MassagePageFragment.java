package com.llw.changeavatar.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.llw.changeavatar.Activity.Login.MainActivity;
import com.llw.changeavatar.Activity.Login.MassageActivity;
import com.llw.changeavatar.Activity.Login.SignUp;
import com.llw.changeavatar.Adapter.BuyListAdapter;
import com.llw.changeavatar.R;
import com.llw.changeavatar.bean.BuyListBean;
import com.llw.changeavatar.bean.HomeListBean;
import com.llw.changeavatar.utils.Const;
import com.llw.changeavatar.utils.Myconfig;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.LinkedList;
import java.util.List;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MassagePageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MassagePageFragment extends Fragment  {
    private static int count = 10;
    private static int offset = 0;
    BuyListAdapter adapter;
    RecyclerView rvRefresh;
    SmartRefreshLayout srlControl;
    View view;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MassagePageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        smartRefresh();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MassagePageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MassagePageFragment newInstance(String param1, String param2) {
        MassagePageFragment fragment = new MassagePageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        if(view==null) {
            view = inflater.inflate(R.layout.fragment_massage_page, container, false);
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(adapter == null || adapter.BuyListData.size()==0){
                    rvRefresh = (RecyclerView)view.findViewById(R.id.home_recycler1);
                    srlControl = (SmartRefreshLayout) view.findViewById(R.id.srl_control1);
                    LinkedList<BuyListBean.DataBean> dataByNothing = getDataByNothing(offset, count);
                    if (getActivity()!=null){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                recyclerRefresh(getActivity(),dataByNothing);
                                smartRefresh();
                            }
                        });
                    }
                }
            }
        }).start();

        return view;
    }
    public void recyclerRefresh(Context context, List<BuyListBean.DataBean> Data){
        adapter = new BuyListAdapter(context, Data);
        rvRefresh.setLayoutManager(new GridLayoutManager(context, 1));
        rvRefresh.setAdapter(adapter);
        rvRefresh.setNestedScrollingEnabled(false);
    }

    //监听下拉和上拉状态
    public void smartRefresh(){
        //下拉刷新
        srlControl.setOnRefreshListener(refreshlayout -> {
            srlControl.setEnableRefresh(true);//启用刷新
            /**
             * 正常来说，应该在这里加载网络数据
             * 这里我们就使用模拟数据 Data() 来模拟我们刷新出来的数据
             */
//            adapter.homeListData.clear();
            new Thread(new Runnable(){
                @Override
                public void run() {
                    LinkedList<BuyListBean.DataBean> dataByNothing = getDataByNothing(0, adapter.BuyListData.size());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.BuyListData.clear();
                            adapter.refreshData(dataByNothing);
                        }
                    });
                }
            }).start();
            srlControl.finishRefresh();//结束刷新
        });
        //上拉加载
        srlControl.setOnLoadmoreListener(refreshlayout -> {
            srlControl.setEnableLoadmore(true);//启用加载
            /**
             * 正常来说，应该在这里加载网络数据
             * 这里我们就使用模拟数据 MoreDatas() 来模拟我们加载出来的数据
             */
            new Thread(new Runnable(){
                @Override
                public void run() {
                    LinkedList<BuyListBean.DataBean> dataByNothing = getDataByNothing(adapter.BuyListData.size(), count);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.loadMore(dataByNothing);
                        }
                    });
                }
            }).start();
            srlControl.finishLoadmore();//结束加载
        });
    }


    LinkedList<BuyListBean.DataBean> getDataByNothing(int offset,int count){
        LinkedList<BuyListBean.DataBean> res = new LinkedList<>();
        if(Myconfig.getValue("user.id").length()==0){
            startActivity(new Intent(getContext(), MainActivity.class));
        }
        String value = Myconfig.getValue("user.id");
        String body = HttpUtil.createGet(Const.PUBLIC_SERVER + "/api/order/queryByUserId?userid=" + value+"&offset=" + offset + "&count=" + count).execute().body().trim();
        JSONArray data = new JSONObject(body).getJSONArray("data");
        int len =data.size();
        for (int i = 0; i < len; i++) {
            JSONObject item = data.getJSONObject(i);
            JSONArray itemlist=item.getJSONArray("order_items");
            String str = item.getJSONArray("order_items").getJSONObject(0).getStr("name");
            if(itemlist.size()>1){
                str+=String.format(" 等%d个商品",itemlist.size());
            }
            res.add(new BuyListBean.DataBean(itemlist.toString(), item.getInt("id"),item.getStr("order_name"),str, item.getInt("ispay"), item.getInt("order_price").toString(), item.getInt("shop_id"),item.getInt("user_id"),item.getStr("shop_url")));
        }
        return res;
    }
}