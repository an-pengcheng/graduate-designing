package com.llw.changeavatar.bean;

public class TalkBean {
    public static final int TYPE_RECEIVED = 0;//我们设置发送者标签为0
    public static final int TYPE_SEND = 1;//我们设置接收者的标签为1
    private String content;//对话内容
    private int type;//标签
    private String pic_shop;
    private String pic_user;

    public TalkBean(String content, int type, String pic_shop, String pic_user) {
        this.content = content;
        this.type = type;
        this.pic_shop = pic_shop;
        this.pic_user = pic_user;
    }

    public static int getTypeReceived() {
        return TYPE_RECEIVED;
    }

    public static int getTypeSend() {
        return TYPE_SEND;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setPic_shop(String pic_shop) {
        this.pic_shop = pic_shop;
    }

    public void setPic_user(String pic_user) {
        this.pic_user = pic_user;
    }

    public String getPic_shop() {
        return pic_shop;
    }

    public String getPic_user() {
        return pic_user;
    }

    public String getContent() {
        return content;
    }

    public int getType() {
        return type;
    }
}