package com.llw.changeavatar.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MassageListBean {
    @SerializedName("data")
    private List<MassageListBean.DataBean> data;
    @SerializedName("msg")
    private String msg;

    public List<MassageListBean.DataBean> getData() {
        return data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setData(List<MassageListBean.DataBean> data) {
        this.data = data;
    }
    public static class DataBean {
        @SerializedName("massage_time")
        private String massage_time;
        @SerializedName("massage_pic")
        private String massage_pic;
        @SerializedName("massage_content")
        private String massage_content;
        @SerializedName("massage_state")
        private String massage_state;
        @SerializedName("massage_name")
        private String massage_name;
        @SerializedName("massage_shopid")
        private int massage_shopid;
        @SerializedName("massage_userid")
        private int massage_userid;

        public DataBean(String massage_time, String massage_pic, String massage_content, String massage_state, String massage_name, int massage_shopid, int massage_userid) {
            this.massage_time = massage_time;
            this.massage_pic = massage_pic;
            this.massage_content = massage_content;
            this.massage_state = massage_state;
            this.massage_name = massage_name;
            this.massage_shopid = massage_shopid;
            this.massage_userid = massage_userid;
        }

        public void setMassage_name(String massage_name) {
            this.massage_name = massage_name;
        }

        public String getMassage_name() {
            return massage_name;
        }

        public void setMassage_time(String massage_time) {
            this.massage_time = massage_time;
        }

        public void setMassage_pic(String massage_pic) {
            this.massage_pic = massage_pic;
        }

        public void setMassage_content(String massage_content) {
            this.massage_content = massage_content;
        }

        public void setMassage_state(String massage_state) {
            this.massage_state = massage_state;
        }

        public void setMassage_shopid(Integer massage_shopid) {
            this.massage_shopid = massage_shopid;
        }

        public void setMassage_userid(Integer massage_userid) {
            this.massage_userid = massage_userid;
        }

        public String getMassage_time() {
            return massage_time;
        }

        public String getMassage_pic() {
            return massage_pic;
        }

        public String getMassage_content() {
            return massage_content;
        }

        public String getMassage_state() {
            return massage_state;
        }

        public int getMassage_shopid() {
            return massage_shopid;
        }

        public int getMassage_userid() {
            return massage_userid;
        }
    }
}
