package com.llw.changeavatar.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AssListBean {

    @SerializedName("data")
    private List<AssListBean.DataBean> data;
    @SerializedName("msg")
    private String msg;

    public List<AssListBean.DataBean> getData() {
        return data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setData(List<AssListBean.DataBean> data) {
        this.data = data;
    }
    public static class DataBean {
        @SerializedName("star1")
        private int star1;
        @SerializedName("star2")
        private int star2;
        @SerializedName("star3")
        private int star3;
        @SerializedName("user_ass_pic")
        private String user_ass_pic;
        @SerializedName("user_ass_name")
        private String user_ass_name;
        @SerializedName("user_ass_content")
        private String user_ass_content;
        @SerializedName("shop_ass_pic")
        private String shop_ass_pic;
        @SerializedName("shop_ass_name")
        private String shop_ass_name;
        @SerializedName("shop_id")
        private Integer shop_id;

        public DataBean(int star1, int star2, int star3, String user_ass_pic, String user_ass_name, String user_ass_content, String shop_ass_pic, String shop_ass_name) {
            this.star1 = star1;
            this.star2 = star2;
            this.star3 = star3;
            this.user_ass_pic = user_ass_pic;
            this.user_ass_name = user_ass_name;
            this.user_ass_content = user_ass_content;
            this.shop_ass_pic = shop_ass_pic;
            this.shop_ass_name = shop_ass_name;
        }
        public DataBean(int star1, int star2, int star3, String user_ass_pic, String user_ass_name, String user_ass_content, String shop_ass_pic, String shop_ass_name,Integer shop_id) {
            this.star1 = star1;
            this.star2 = star2;
            this.star3 = star3;
            this.user_ass_pic = user_ass_pic;
            this.user_ass_name = user_ass_name;
            this.user_ass_content = user_ass_content;
            this.shop_ass_pic = shop_ass_pic;
            this.shop_ass_name = shop_ass_name;
            this.shop_id = shop_id;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "star1=" + star1 +
                    ", star2=" + star2 +
                    ", star3=" + star3 +
                    ", user_ass_pic='" + user_ass_pic + '\'' +
                    ", user_ass_name='" + user_ass_name + '\'' +
                    ", user_ass_content='" + user_ass_content + '\'' +
                    ", shop_ass_pic='" + shop_ass_pic + '\'' +
                    ", shop_ass_name='" + shop_ass_name + '\'' +
                    '}';
        }

        public void setStar1(int star1) {
            this.star1 = star1;
        }

        public void setStar2(int star2) {
            this.star2 = star2;
        }

        public void setStar3(int star3) {
            this.star3 = star3;
        }

        public void setUser_ass_pic(String user_ass_pic) {
            this.user_ass_pic = user_ass_pic;
        }

        public void setUser_ass_name(String user_ass_name) {
            this.user_ass_name = user_ass_name;
        }

        public void setUser_ass_content(String user_ass_content) {
            this.user_ass_content = user_ass_content;
        }

        public void setShop_ass_pic(String shop_ass_pic) {
            this.shop_ass_pic = shop_ass_pic;
        }

        public void setShop_ass_name(String shop_ass_name) {
            this.shop_ass_name = shop_ass_name;
        }

        public int getStar1() {
            return star1;
        }

        public int getStar2() {
            return star2;
        }

        public int getStar3() {
            return star3;
        }

        public String getUser_ass_pic() {
            return user_ass_pic;
        }

        public String getUser_ass_name() {
            return user_ass_name;
        }

        public String getUser_ass_content() {
            return user_ass_content;
        }

        public String getShop_ass_pic() {
            return shop_ass_pic;
        }

        public String getShop_ass_name() {
            return shop_ass_name;
        }

        public Integer getShop_id() {
            return shop_id;
        }

        public void setShop_id(Integer shop_id) {
            this.shop_id = shop_id;
        }
    }
}
