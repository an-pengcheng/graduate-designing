package com.llw.changeavatar.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShopAssListBean {
    @SerializedName("data")
    private List<ShopAssListBean.DataBean> data;
    @SerializedName("msg")
    private String msg;

    public List<ShopAssListBean.DataBean> getData() {
        return data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setData(List<ShopAssListBean.DataBean> data) {
        this.data = data;
    }
    public static class DataBean {
        @SerializedName("shop_ass_user_id")
        private int shop_ass_user_id;
        @SerializedName("shop_id")
        private int shop_id;
        @SerializedName("shop_ass_name")
        private String shop_ass_name;
        @SerializedName("shop_ass_user_pic")
        private String shop_ass_user_pic;
        @SerializedName("shop_user_ass_stars")
        private String shop_user_ass_stars;
        @SerializedName("shop_user_ass_content")
        private String shop_user_ass_content;

        public DataBean(int shop_ass_user_id, int shop_id, String shop_ass_name, String shop_ass_user_pic, String shop_user_ass_stars, String shop_user_ass_content) {
            this.shop_ass_user_id = shop_ass_user_id;
            this.shop_id = shop_id;
            this.shop_ass_name = shop_ass_name;
            this.shop_ass_user_pic = shop_ass_user_pic;
            this.shop_user_ass_stars = shop_user_ass_stars;
            this.shop_user_ass_content = shop_user_ass_content;
        }

        public String getShop_ass_name() {
            return shop_ass_name;
        }

        public void setShop_ass_name(String shop_ass_name) {
            this.shop_ass_name = shop_ass_name;
        }

        public void setShop_ass_user_id(int shop_ass_user_id) {
            this.shop_ass_user_id = shop_ass_user_id;
        }

        public void setShop_id(int shop_id) {
            this.shop_id = shop_id;
        }

        public void setShop_ass_user_pic(String shop_ass_user_pic) {
            this.shop_ass_user_pic = shop_ass_user_pic;
        }

        public void setShop_user_ass_stars(String shop_user_ass_stars) {
            this.shop_user_ass_stars = shop_user_ass_stars;
        }

        public void setShop_user_ass_content(String shop_user_ass_content) {
            this.shop_user_ass_content = shop_user_ass_content;
        }

        public int getShop_ass_user_id() {
            return shop_ass_user_id;
        }

        public int getShop_id() {
            return shop_id;
        }


        public String getShop_ass_user_pic() {
            return shop_ass_user_pic;
        }

        public String getShop_user_ass_stars() {
            return shop_user_ass_stars;
        }

        public String getShop_user_ass_content() {
            return shop_user_ass_content;
        }
    }
}
