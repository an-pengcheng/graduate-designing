package com.llw.changeavatar.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PurchaseListBean {
    @SerializedName("data")
    private List<PurchaseListBean.DataBean> data;
    @SerializedName("msg")
    private String msg;
    public List<PurchaseListBean.DataBean> getData() {
        return data;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public void setData(List<PurchaseListBean.DataBean> data) {
        this.data = data;
    }
    public static class DataBean {

        private int count;
        private int nowprice;
        private String item_url;
        private String name;
        private String context;
        private String order_price;

        public int getCount() {
            return count;
        }

        public int getNowprice() {
            return nowprice;
        }

        public String getItem_url() {
            return item_url;
        }

        public String getName() {
            return name;
        }

        public String getContext() {
            return context;
        }

        public String getOrder_price() {
            return order_price;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public void setNowprice(int nowprice) {
            this.nowprice = nowprice;
        }

        public void setItem_url(String item_url) {
            this.item_url = item_url;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setContext(String context) {
            this.context = context;
        }

        public void setOrder_price(String order_price) {
            this.order_price = order_price;
        }

        public DataBean(String item_url, String name, String context, int count, int nowprice){
            this.item_url=item_url;
            this.name=name;
            this.context=context;
            this.count=count;
            this.nowprice=nowprice;
        }
    }
}
