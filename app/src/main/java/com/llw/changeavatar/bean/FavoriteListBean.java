package com.llw.changeavatar.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FavoriteListBean {

    @SerializedName("data")
    private List<FavoriteListBean.DataBean> data;
    @SerializedName("msg")
    private String msg;

    public List<FavoriteListBean.DataBean> getData() {
        return data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setData(List<FavoriteListBean.DataBean> data) {
        this.data = data;
    }
    public static class DataBean {
        @SerializedName("fav_shop_id")
        private Integer fav_shop_id;
        @SerializedName("fav_user_id")
        private Integer fav_user_id;
        @SerializedName("fav_shop_pic")
        private String fav_shop_pic;
        @SerializedName("fav_shop_name")
        private String fav_shop_name;
        @SerializedName("fav_shop_place")
        private String fav_shop_place;
        @SerializedName("fav_shop_type")
        private String fav_shop_type;
        @SerializedName("fav_shop_main")
        private String fav_shop_main;

        public void setFav_shop_main(String fav_shop_main) {
            this.fav_shop_main = fav_shop_main;
        }

        public String getFav_shop_main() {
            return fav_shop_main;
        }

        public void setFav_shop_id(Integer fav_shop_id) {
            this.fav_shop_id = fav_shop_id;
        }

        public void setFav_user_id(Integer fav_user_id) {
            this.fav_user_id = fav_user_id;
        }

        public void setFav_shop_pic(String fav_shop_pic) {
            this.fav_shop_pic = fav_shop_pic;
        }

        public void setFav_shop_name(String fav_shop_name) {
            this.fav_shop_name = fav_shop_name;
        }

        public void setFav_shop_place(String fav_shop_place) {
            this.fav_shop_place = fav_shop_place;
        }

        public void setFav_shop_type(String fav_shop_type) {
            this.fav_shop_type = fav_shop_type;
        }

        public Integer getFav_shop_id() {
            return fav_shop_id;
        }

        public Integer getFav_user_id() {
            return fav_user_id;
        }

        public String getFav_shop_pic() {
            return fav_shop_pic;
        }

        public String getFav_shop_name() {
            return fav_shop_name;
        }

        public String getFav_shop_place() {
            return fav_shop_place;
        }

        public String getFav_shop_type() {
            return fav_shop_type;
        }

        public DataBean(Integer fav_shop_id, Integer fav_user_id, String fav_shop_pic, String fav_shop_name, String fav_shop_place, String fav_shop_type, String fav_shop_main) {
            this.fav_shop_id = fav_shop_id;
            this.fav_user_id = fav_user_id;
            this.fav_shop_pic = fav_shop_pic;
            this.fav_shop_name = fav_shop_name;
            this.fav_shop_place = fav_shop_place;
            this.fav_shop_type = fav_shop_type;
            this.fav_shop_main = fav_shop_main;
        }
    }
}
