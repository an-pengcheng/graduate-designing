package com.llw.changeavatar.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BuyListBean {

    @SerializedName("data")
    private List<DataBean> data;
    @SerializedName("msg")
    private String msg;

    public List<DataBean> getData() {
        return data;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        @SerializedName("order_items")
        private String  order_items;
        @SerializedName("id")
        private int id;
        @SerializedName("order_name")
        private String order_name;
        @SerializedName("order_items")
        private String order_item;
        @SerializedName("ispay")
        private int ispay;
        @SerializedName("order_price")
        private String order_price;
        @SerializedName("shop_id")
        private int shop_id;
        @SerializedName("user_id")
        private int user_id;
        @SerializedName("pic_url")
        private String pic_url;
        @SerializedName("shop_name")
        private String shop_name;

        private int count;
        private int nowprice;
        private String item_url;
        private String name;
        private String context;


        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public String getShop_name() {
            return shop_name;
        }

        public String getOrder_items() {
            return order_items;
        }

        public void setOrder_items(String order_items) {
            this.order_items = order_items;
        }

        public String getPic_url() {
            return pic_url;
        }

        public void setIspay(int ispay) {
            this.ispay = ispay;
        }

        public void setShop_id(int shop_id) {
            this.shop_id = shop_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public void setPic_url(String pic_url) {
            this.pic_url = pic_url;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setOrder_name(String order_name) {
            this.order_name = order_name;
        }

        public void setOrder_item(String order_item) {
            this.order_item = order_item;
        }

        public void setOrder_price(String order_price) {
            this.order_price = order_price;
        }

        public int getId() {
            return id;
        }

        public String getOrder_name() {
            return order_name;
        }

        public String getOrder_item() {
            return order_item;
        }

        public int getIspay() {
            return ispay;
        }

        public String getOrder_price() {
            return order_price;
        }

        public int getShop_id() {
            return shop_id;
        }

        public int getUser_id() {
            return user_id;
        }

        public DataBean(String order_items,int id, String order_name, String order_item, int ispay, String order_price, int shop_id, int user_id,String pic_url) {
            this.id = id;
            this.order_items=order_items;
            this.order_name = order_name;
            this.order_item = order_item;
            this.ispay = ispay;
            this.order_price = order_price;
            this.shop_id = shop_id;
            this.user_id = user_id;
            this.pic_url = pic_url;
        }
        public DataBean(String item_url,String name,String context,int count,int nowprice,String order_price){
            this.item_url=item_url;
            this.name=name;
            this.context=context;
            this.count=count;
            this.nowprice=nowprice;
            this.order_price = order_price;
        }

    }
}
