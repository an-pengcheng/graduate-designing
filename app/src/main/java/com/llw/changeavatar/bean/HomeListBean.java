package com.llw.changeavatar.bean;


import com.google.gson.annotations.SerializedName;

import java.util.List;

import cn.hutool.core.util.StrUtil;

public class HomeListBean {

    @SerializedName("data")
    private List<DataBean> data;
    @SerializedName("msg")
    private String msg;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        @SerializedName("deliver")
        private String deliver;
        @SerializedName("deliverTime")
        private String deliverTime;
        @SerializedName("detailUrl")
        private String detailUrl;
        @SerializedName("id")
        private int id;
        @SerializedName("lable")
        private String lable;
        @SerializedName("sale")
        private String sale;
        @SerializedName("title")
        private String title;

        public String getDeliver() {
            return deliver;
        }

        public void setDeliver(String deliver) {
            this.deliver = deliver;
        }

        public String getDeliverTime() {
            if(StrUtil.isBlank(deliverTime))return "1";
            return deliverTime;
        }

        public void setDeliverTime(String deliverTime) {
            this.deliverTime = deliverTime;
        }

        public String getDetailUrl() {
            return detailUrl;
        }

        public void setDetailUrl(String detailUrl) {
            this.detailUrl = detailUrl;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getLable() {
            return lable;
        }

        public void setLable(String lable) {
            this.lable = lable;
        }

        public String getSale() {
            return sale;
        }

        public void setSale(String sale) {
            this.sale = sale;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public DataBean(String deliver, String deliverTime, String detailUrl, int id, String lable, String sale, String title) {
            this.deliver = deliver;
            this.deliverTime = deliverTime;
            this.detailUrl = detailUrl;
            this.id = id;
            this.lable = lable;
            this.sale = sale;
            this.title = title;
        }
        public DataBean() {
        }
    }
}
